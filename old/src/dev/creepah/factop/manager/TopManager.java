package dev.creepah.factop.manager;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;
import dev.creepah.factop.FactionsTop;
import org.bukkit.Bukkit;
import org.bukkit.ChunkSnapshot;
import org.bukkit.plugin.Plugin;

import java.util.*;
import java.util.stream.Collectors;

public class TopManager implements Runnable {
    private final Object $lock;
    public final Map<Faction, FactionResult> topFactions;
    boolean firstRun;
    public List<Faction> factionsToCheck;

    public TopManager() {
        this.$lock = new Object[0];
        this.topFactions = new HashMap<Faction, FactionResult>();
        this.firstRun = true;
        this.factionsToCheck = new ArrayList<Faction>();
        final int refresh = FactionsTop.getInstance().getConfig().getInt("refresh");
        Bukkit.getScheduler().runTaskTimer((Plugin) FactionsTop.getInstance(), (Runnable) this, 20L, (long) (refresh * 20));
    }

    @Override
    public void run() {
        int checked = 0;
        int chunks = 0;

        for (final Faction faction : this.firstRun ? Factions.i.get() : this.factionsToCheck) {
            if (faction != Factions.i.getNone() && faction.isNormal()) {
                if (faction.isSafeZone()) {
                    continue;
                }
                final Set<ChunkSnapshot> snapshots = new HashSet<ChunkSnapshot>();
                snapshots.addAll(Board.getFactionClaims(faction).stream().map(f -> Bukkit.getWorld(f.getWorldName()).getChunkAt((int) f.getX(), (int) f.getZ()).getChunkSnapshot()).collect(Collectors.toList()));
                Bukkit.getScheduler().runTaskAsynchronously((Plugin) FactionsTop.getInstance(), () -> {
                    FactionResult result = new FactionResult(faction, snapshots);
                    this.putValue(faction, result);
                    return;
                });
                ++checked;
                chunks += snapshots.size();
            }
        }

        if (this.firstRun) {
            this.firstRun = false;
        }

        this.factionsToCheck.clear();
        FactionsTop.getInstance().getLogger().info(String.valueOf(chunks) + " chunks scanned across " + checked + " factions!");
    }

    public List<FactionResult> getFactionResults() {
        final List<FactionResult> results = new ArrayList<FactionResult>(this.getAllResults());
        Collections.sort(results, (o1, o2) -> (int) (o2.getValue() - o1.getValue()));
        return results;
    }

    public Collection<FactionResult> getAllResults() {
        synchronized (this.$lock) {
            // monitorexit(this.$lock)
            return this.topFactions.values();
        }
    }

    private void putValue(final Faction faction, final FactionResult result) {
        synchronized (this.$lock) {
            final FactionResult value = this.getValue(faction);
            if (value != null) {
                if (value.getValue() != result.getValue()) {
                    this.topFactions.replace(faction, result);
                }
            } else {
                this.topFactions.put(faction, result);
            }
        }
        // monitorexit(this.$lock)
    }

    public FactionResult getValue(final Faction faction) {
        synchronized (this.$lock) {
            // monitorexit(this.$lock)
            return this.topFactions.get(faction);
        }
    }

    public Map<Faction, FactionResult> getTopFactions() {
        return this.topFactions;
    }
}
