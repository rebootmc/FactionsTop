package dev.creepah.factop.manager;

import org.bukkit.configuration.file.*;
import dev.creepah.factop.*;
import org.bukkit.configuration.*;
import java.util.*;

public class SpawnerConfig
{
    private final Map<String, Integer> spawnerWorthMap;
    FileConfiguration config;
    
    public SpawnerConfig() {
        this.spawnerWorthMap = new HashMap<String, Integer>();
        this.config = FactionsTop.getInstance().getConfig();
        final ConfigurationSection spawners = this.config.getConfigurationSection("spawners");
        for (final String key : spawners.getKeys(false)) {
            final int value = spawners.getInt(key, 0);
            if (value == 0) {
                continue;
            }
            this.spawnerWorthMap.put(key, value);
        }
    }
    
    public int getWorth(final String creatureTypeName) {
        return this.spawnerWorthMap.getOrDefault(creatureTypeName, 0);
    }
}
