package dev.creepah.factop.manager;

import com.massivecraft.factions.Faction;
import dev.creepah.factop.FactionsTop;
import org.bukkit.Bukkit;
import org.bukkit.ChunkSnapshot;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;

import java.util.Set;

public class FactionResult {
    private static final int CHUNK_SIZE = 16;
    private static final int WORLD_HEIGHT = 256;
    private final Faction faction;
    private long value;
    WorthConfig config;
    SpawnerConfig spawnerConfig;
    TopManager manager;

    public FactionResult(final Faction faction, final Set<ChunkSnapshot> snapshots) {
        this.value = 0L;
        this.config = FactionsTop.getInstance().getWorthConfig();
        this.spawnerConfig = FactionsTop.getInstance().getSpawnerConfig();
        this.manager = FactionsTop.getInstance().getManager();
        this.faction = faction;
        long value = 0L;
        for (final ChunkSnapshot snapshot : snapshots) {
            for (int x = 0; x < 16; ++x) {
                for (int y = 0; y < 256 && !snapshot.isSectionEmpty(y >> 4); ++y) {
                    for (int z = 0; z < 16; ++z) {
                        if (snapshot.getBlockTypeId(x, y, z) == 52) {
                            final Block block = Bukkit.getWorld(snapshot.getWorldName()).getBlockAt(snapshot.getX() * 16 + x, y, snapshot.getZ() * 16 + z);
                            if (block.getType() == Material.MOB_SPAWNER) {
                                final CreatureSpawner spawner = (CreatureSpawner) block.getState();
                                value += this.spawnerConfig.getWorth(spawner.getCreatureTypeName().toLowerCase());
                            }
                        } else {
                            value += this.config.getWorth(snapshot.getBlockTypeId(x, y, z));
                        }
                    }
                }
            }
        }
        this.value = value;
    }

    public void setValue(final long value) {
        this.value = value;
    }

    public Faction getFaction() {
        return this.faction;
    }

    public long getValue() {
        return this.value;
    }

    public WorthConfig getConfig() {
        return this.config;
    }

    public SpawnerConfig getSpawnerConfig() {
        return this.spawnerConfig;
    }

    public TopManager getManager() {
        return this.manager;
    }
}
