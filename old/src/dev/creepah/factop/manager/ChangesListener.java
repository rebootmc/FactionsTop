package dev.creepah.factop.manager;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;
import dev.creepah.factop.FactionsTop;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class ChangesListener implements Listener {
    @EventHandler
    public void onBreak(final BlockBreakEvent event) {
        this.check(event.getPlayer(), event.getBlock());
    }

    @EventHandler
    public void onPlace(final BlockPlaceEvent event) {
        this.check(event.getPlayer(), event.getBlock());
    }

    public void check(final Player player, final Block block) {
        final Faction faction = this.hasValidFaction(player);
        if (faction != null && this.isInFactionLand(block, faction)) {
            final FactionsTop instance = FactionsTop.getInstance();
            int value;
            if (block.getType() == Material.MOB_SPAWNER) {
                final CreatureSpawner state = (CreatureSpawner) block.getState();
                value = instance.getSpawnerConfig().getWorth(state.getCreatureTypeName().toLowerCase());
            } else {
                value = instance.getWorthConfig().getWorth(block.getTypeId());
            }
            if (value > 0 && !instance.getManager().factionsToCheck.contains(faction)) {
                instance.getManager().factionsToCheck.add(faction);
            }
        }
    }

    public Faction hasValidFaction(final Player player) {
        final Faction faction = FPlayers.i.get(player).getFaction();
        if (faction != Factions.i.getNone() && faction.isNormal() && !faction.isSafeZone()) {
            return faction;
        }
        return null;
    }

    public boolean isInFactionLand(final Block block, final Faction faction) {
        return Board.getFactionAt(block.getLocation()) == faction;
    }
}
