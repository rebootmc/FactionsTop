package dev.creepah.factop.manager;

import org.bukkit.configuration.file.*;
import dev.creepah.factop.*;
import org.bukkit.*;
import org.bukkit.configuration.*;
import java.util.*;

public class WorthConfig
{
    private final Map<Integer, Integer> internalWorthMap;
    FileConfiguration config;
    
    public WorthConfig() {
        this.internalWorthMap = new HashMap<Integer, Integer>();
        this.config = FactionsTop.getInstance().getConfig();
        final ConfigurationSection blocks = this.config.getConfigurationSection("blocks");
        for (final String key : blocks.getKeys(false)) {
            Material material;
            try {
                material = Material.valueOf(key.toUpperCase());
            }
            catch (Exception e) {
                continue;
            }
            final int value = blocks.getInt(key, 0);
            if (value == 0) {
                continue;
            }
            this.internalWorthMap.put(material.getId(), value);
        }
    }
    
    public int getWorth(final int block) {
        return this.internalWorthMap.getOrDefault(block, 0);
    }
}
