package dev.creepah.factop.cmd;

import dev.creepah.factop.FactionsTop;
import dev.creepah.factop.manager.FactionResult;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.text.NumberFormat;
import java.util.List;

public class TopCommand implements Listener {
    @EventHandler
    public void onCommand(final PlayerCommandPreprocessEvent event) {
        final String message = event.getMessage();
        if (message.toLowerCase().contains("/f top")) {
            final FactionsTop instance = FactionsTop.getInstance();
            final List<FactionResult> results = instance.getManager().getFactionResults();
            int page = 1;
            final int parsed = this.parse(message.substring(message.length() - 1, message.length()));
            if (parsed > 0) {
                page = parsed;
            }
            event.getPlayer().sendMessage(instance.formatAt("list-title").with("page", page).get());
            int shown = 0;
            final int n = instance.getConfig().getInt("list-no");
            for (int i = (page - 1) * n; i < results.size(); ++i) {
                if (shown < n) {
                    event.getPlayer().sendMessage(instance.formatAt("list-value").with("position", i + 1).with("name", results.get(i).getFaction().getId()).with("value", this.format(results.get(i).getValue())).get());
                    ++shown;
                }
            }
            event.setCancelled(true);
        }
    }

    public String format(final long value) {
        return NumberFormat.getInstance().format(value);
    }

    public int parse(final String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException ex) {
            return -1;
        }
    }
}
