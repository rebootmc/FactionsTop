package dev.creepah.factop;

import me.twister915.corelite.plugin.*;
import dev.creepah.factop.cmd.*;
import dev.creepah.factop.manager.*;

@UsesFormats
public final class FactionsTop extends CLPlugin
{
    private static FactionsTop instance;
    private WorthConfig worthConfig;
    private SpawnerConfig spawnerConfig;
    private TopManager manager;
    
    @Override
    protected void onModuleEnable() throws Exception {
        FactionsTop.instance = this;
        this.manager = new TopManager();
        this.worthConfig = new WorthConfig();
        this.spawnerConfig = new SpawnerConfig();
        this.registerListener(new TopCommand());
        this.registerListener(new ChangesListener());
    }
    
    public static FactionsTop getInstance() {
        return FactionsTop.instance;
    }
    
    public WorthConfig getWorthConfig() {
        return this.worthConfig;
    }
    
    public SpawnerConfig getSpawnerConfig() {
        return this.spawnerConfig;
    }
    
    public TopManager getManager() {
        return this.manager;
    }
}
