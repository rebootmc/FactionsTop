package me.twister915.corelite.plugin;

import org.bukkit.ChatColor;

import java.util.HashMap;
import java.util.Map;

public final class Formatter
{
    private String loadedPrefix;
    private final YAMLConfigurationFile formatsFile;
    
    public Formatter(final YAMLConfigurationFile formatsFile) {
        this.formatsFile = formatsFile;
        this.loadedPrefix = (formatsFile.getConfig().contains("prefix") ? ChatColor.translateAlternateColorCodes('&', formatsFile.getConfig().getString("prefix")) : null);
    }
    
    public FormatBuilder begin(final String path) {
        return new FormatBuilder(this.formatsFile.getConfig().getString(path));
    }
    
    public boolean has(final String path) {
        return this.formatsFile.getConfig().contains(path);
    }
    
    public FormatBuilder withValue(final String value) {
        return new FormatBuilder(value);
    }
    
    public final class FormatBuilder
    {
        private final String formatString;
        private final Map<String, String> modifiers;
        private boolean prefix;
        private boolean coloredInputs;
        
        private FormatBuilder(final String formatString) {
            this.modifiers = new HashMap<String, String>();
            this.prefix = true;
            this.coloredInputs = true;
            this.formatString = formatString;
        }
        
        public FormatBuilder withModifier(final String key, final Object value) {
            this.modifiers.put(key, value.toString());
            return this;
        }
        
        public FormatBuilder with(final String key, final Object value) {
            this.modifiers.put(key, value.toString());
            return this;
        }
        
        public FormatBuilder withPrefix(final boolean p) {
            this.prefix = p;
            return this;
        }
        
        public FormatBuilder withColoredInputs(final boolean c) {
            this.coloredInputs = c;
            return this;
        }
        
        public String get() {
            if (this.formatString == null) {
                return "Not found!";
            }
            String s = ChatColor.translateAlternateColorCodes('&', this.formatString);
            for (final Map.Entry<String, String> stringStringEntry : this.modifiers.entrySet()) {
                String value = stringStringEntry.getValue();
                if (this.coloredInputs) {
                    value = ChatColor.translateAlternateColorCodes('&', value);
                }
                s = s.replaceAll(String.format("\\{\\{%s\\}\\}", stringStringEntry.getKey()), value);
            }
            if (this.prefix && Formatter.this.loadedPrefix != null) {
                return String.valueOf(Formatter.this.loadedPrefix) + s;
            }
            return s;
        }
    }
}
