package me.twister915.corelite.plugin;

import java.lang.annotation.*;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface UsesFormats {
    String file() default "formats.yml";
}
