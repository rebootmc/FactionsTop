package me.twister915.corelite.plugin;

import org.bukkit.plugin.java.*;
import lombok.*;
import org.bukkit.configuration.file.*;
import org.bukkit.configuration.*;
import java.util.logging.*;
import java.io.*;

public final class YAMLConfigurationFile
{
    private final String fileName;
    private final JavaPlugin plugin;
    private File configFile;
    private FileConfiguration fileConfiguration;
    
    public YAMLConfigurationFile(@NonNull final JavaPlugin plugin, @NonNull final String fileName) {
        this(plugin, new File(plugin.getDataFolder(), fileName));
        if (plugin == null) {
            throw new NullPointerException("plugin");
        }
        if (fileName == null) {
            throw new NullPointerException("fileName");
        }
    }
    
    public YAMLConfigurationFile(@NonNull final JavaPlugin plugin, final File file) {
        if (plugin == null) {
            throw new NullPointerException("plugin");
        }
        if (!plugin.isEnabled()) {
            throw new IllegalArgumentException("plugin must be enabled");
        }
        this.plugin = plugin;
        this.fileName = file.getName();
        final File dataFolder = plugin.getDataFolder();
        if (dataFolder == null) {
            throw new IllegalStateException();
        }
        this.configFile = file;
    }
    
    public void reloadConfig() {
        this.fileConfiguration = (FileConfiguration)YamlConfiguration.loadConfiguration(this.configFile);
        final InputStream defConfigStream = this.plugin.getResource(this.fileName);
        if (defConfigStream != null) {
            final YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            this.fileConfiguration.setDefaults((Configuration)defConfig);
        }
    }
    
    public FileConfiguration getConfig() {
        if (this.fileConfiguration == null) {
            this.reloadConfig();
        }
        return this.fileConfiguration;
    }
    
    public void saveConfig() {
        if (this.fileConfiguration == null || this.configFile == null) {
            return;
        }
        try {
            this.getConfig().save(this.configFile);
        }
        catch (IOException ex) {
            this.plugin.getLogger().log(Level.SEVERE, "Could not save config to " + this.configFile, ex);
        }
    }
    
    public void saveDefaultConfig() {
        if (!this.configFile.exists()) {
            this.plugin.saveResource(this.fileName, false);
        }
    }
    
    public String getFileName() {
        return this.fileName;
    }
    
    public JavaPlugin getPlugin() {
        return this.plugin;
    }
    
    public File getConfigFile() {
        return this.configFile;
    }
    
    public FileConfiguration getFileConfiguration() {
        return this.fileConfiguration;
    }
    
    public void setConfigFile(final File configFile) {
        this.configFile = configFile;
    }
    
    public void setFileConfiguration(final FileConfiguration fileConfiguration) {
        this.fileConfiguration = fileConfiguration;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof YAMLConfigurationFile)) {
            return false;
        }
        final YAMLConfigurationFile other = (YAMLConfigurationFile)o;
        final Object this$fileName = this.getFileName();
        final Object other$fileName = other.getFileName();
        Label_0058: {
            if (this$fileName == null) {
                if (other$fileName == null) {
                    break Label_0058;
                }
            }
            else if (this$fileName.equals(other$fileName)) {
                break Label_0058;
            }
            return false;
        }
        final Object this$plugin = this.getPlugin();
        final Object other$plugin = other.getPlugin();
        Label_0098: {
            if (this$plugin == null) {
                if (other$plugin == null) {
                    break Label_0098;
                }
            }
            else if (this$plugin.equals(other$plugin)) {
                break Label_0098;
            }
            return false;
        }
        final Object this$configFile = this.getConfigFile();
        final Object other$configFile = other.getConfigFile();
        Label_0138: {
            if (this$configFile == null) {
                if (other$configFile == null) {
                    break Label_0138;
                }
            }
            else if (this$configFile.equals(other$configFile)) {
                break Label_0138;
            }
            return false;
        }
        final Object this$fileConfiguration = this.getFileConfiguration();
        final Object other$fileConfiguration = other.getFileConfiguration();
        if (this$fileConfiguration == null) {
            if (other$fileConfiguration == null) {
                return true;
            }
        }
        else if (this$fileConfiguration.equals(other$fileConfiguration)) {
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $fileName = this.getFileName();
        result = result * 59 + (($fileName == null) ? 0 : $fileName.hashCode());
        final Object $plugin = this.getPlugin();
        result = result * 59 + (($plugin == null) ? 0 : $plugin.hashCode());
        final Object $configFile = this.getConfigFile();
        result = result * 59 + (($configFile == null) ? 0 : $configFile.hashCode());
        final Object $fileConfiguration = this.getFileConfiguration();
        result = result * 59 + (($fileConfiguration == null) ? 0 : $fileConfiguration.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "YAMLConfigurationFile(fileName=" + this.getFileName() + ", plugin=" + this.getPlugin() + ", configFile=" + this.getConfigFile() + ", fileConfiguration=" + this.getFileConfiguration() + ")";
    }
}
