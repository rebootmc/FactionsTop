package me.twister915.corelite.plugin;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import me.twister915.corelite.command.CLCommand;
import me.twister915.corelite.command.CommandMeta;
import org.bukkit.Bukkit;
import org.bukkit.command.*;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

public abstract class CLPlugin extends JavaPlugin {
    private Formatter formatter;
    private Gson gson;

    public CLPlugin() {
        this.gson = this.getNewGson();
    }

    protected Gson getNewGson() {
        return this.getGsonBuilder().create();
    }

    protected GsonBuilder getGsonBuilder() {
        return new GsonBuilder();
    }

    public final void onEnable() {
        try {
            this.saveDefaultConfig();
            if (this.getClass().isAnnotationPresent(UsesFormats.class)) {
                final YAMLConfigurationFile formatsFile = new YAMLConfigurationFile(this, this.getClass().getAnnotation(UsesFormats.class).file());
                formatsFile.saveDefaultConfig();
                this.formatter = new Formatter(formatsFile);
            } else {
                this.formatter = null;
            }
            this.onModuleEnable();
        } catch (Exception e) {
            this.getLogger().severe("Unable to properly enable this plugin!");
            this.setEnabled(false);
            e.printStackTrace();
        }
    }

    public final Formatter.FormatBuilder formatAt(final String path) {
        if (this.formatter == null) {
            throw new IllegalStateException("This plugin is not marked to use formats!");
        }
        return this.formatter.begin(path);
    }

    public final void onDisable() {
        try {
            this.onModuleDisable();
        } catch (Exception e) {
            this.getLogger().severe("Unable to properly disable this plugin!");
            e.printStackTrace();
        }
    }

    public final <T extends Listener> T registerListener(final T listener) {
        this.getServer().getPluginManager().registerEvents((Listener) listener, (Plugin) this);
        return listener;
    }

    public final <T extends CLCommand> T registerCommand(final T command) {
        PluginCommand command2;
        try {
            final Constructor commandConstructor = PluginCommand.class.getDeclaredConstructor(String.class, Plugin.class);
            commandConstructor.setAccessible(true);
            command2 = (PluginCommand) commandConstructor.newInstance(command.getName(), this);
        } catch (Exception ex) {
            throw new IllegalStateException("Could not register command " + command.getName());
        }
        command2.setExecutor((CommandExecutor) command);
        command2.setTabCompleter((TabCompleter) command);
        final CommandMeta annotation = command.getClass().getAnnotation(CommandMeta.class);
        if (annotation != null) {
            command2.setAliases((List) Arrays.asList(annotation.aliases()));
            command2.setDescription(annotation.description());
            command2.setUsage(annotation.usage());
        }
        CommandMap commandMap;
        try {
            final PluginManager pluginManager = Bukkit.getPluginManager();
            final Field commandMapField = pluginManager.getClass().getDeclaredField("commandMap");
            commandMapField.setAccessible(true);
            commandMap = (CommandMap) commandMapField.get(pluginManager);
        } catch (Exception ex2) {
            throw new IllegalStateException("Could not register command " + command.getName());
        }
        commandMap.register(this.getDescription().getName(), (Command) command2);
        return command;
    }

    public void registerListener(final Listener... listeners) {
        for (final Listener listener : listeners) {
            this.registerListener(listener);
        }
    }

    public void regsiterCommand(final CLCommand... commands) {
        for (final CLCommand command : commands) {
            this.registerCommand(command);
        }
    }

    protected void onModuleEnable() throws Exception {
    }

    protected void onModuleDisable() throws Exception {
    }

    public Formatter getFormatter() {
        return this.formatter;
    }

    public Gson getGson() {
        return this.gson;
    }
}
