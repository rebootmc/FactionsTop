package me.twister915.corelite.command;

public interface FriendlyException
{
    String getFriendlyMessage(final CLCommand p0);
}
