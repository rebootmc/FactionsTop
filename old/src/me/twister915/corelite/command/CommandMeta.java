package me.twister915.corelite.command;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface CommandMeta {
    String description() default "";
    
    String[] aliases() default {};
    
    String usage() default "";
}
