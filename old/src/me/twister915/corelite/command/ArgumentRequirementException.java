package me.twister915.corelite.command;

import org.bukkit.*;

public final class ArgumentRequirementException extends CommandException implements FriendlyException
{
    public ArgumentRequirementException(final String message) {
        super(message);
    }
    
    @Override
    public String getFriendlyMessage(final CLCommand command) {
        return ChatColor.RED + this.getMessage();
    }
}
