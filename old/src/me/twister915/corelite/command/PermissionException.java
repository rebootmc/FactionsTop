package me.twister915.corelite.command;

import org.bukkit.*;

public final class PermissionException extends CommandException implements FriendlyException
{
    public PermissionException(final String message) {
        super(message);
    }
    
    @Override
    public String getFriendlyMessage(final CLCommand command) {
        return ChatColor.RED + "You do not have permission for this!";
    }
}
