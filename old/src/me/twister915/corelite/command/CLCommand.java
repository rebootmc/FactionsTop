package me.twister915.corelite.command;

import com.google.common.collect.*;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.*;
import org.bukkit.entity.Player;

import java.util.*;

public abstract class CLCommand implements CommandExecutor, TabCompleter {
    private final Map<String, CLCommand> subCommands;
    private final String name;
    private CLCommand superCommand;
    private CommandMeta meta;

    protected CLCommand(final String name) {
        this.subCommands = new HashMap<String, CLCommand>();
        this.superCommand = null;
        this.meta = (this.getClass().isAnnotationPresent(CommandMeta.class) ? this.getClass().getAnnotation(CommandMeta.class) : null);
        this.name = name;
    }

    protected CLCommand(final String name, final CLCommand... subCommands) {
        this.subCommands = new HashMap<String, CLCommand>();
        this.superCommand = null;
        this.meta = (this.getClass().isAnnotationPresent(CommandMeta.class) ? this.getClass().getAnnotation(CommandMeta.class) : null);
        this.name = name;
        this.registerSubCommand(subCommands);
    }

    public final void registerSubCommand(final CLCommand... subCommands) {
        for (final CLCommand subCommand : subCommands) {
            if (subCommand.getSuperCommand() != null) {
                throw new IllegalArgumentException("The command you attempted to register already has a supercommand.");
            }
            this.subCommands.put(subCommand.getName(), subCommand);
            final CommandMeta meta = subCommand.getMeta();
            if (meta != null && meta.aliases() != null) {
                String[] aliases;
                for (int length2 = (aliases = meta.aliases()).length, j = 0; j < length2; ++j) {
                    final String a = aliases[j];
                    this.subCommands.put(a, subCommand);
                }
            }
            subCommand.setSuperCommand(this);
        }
        this.regenerateHelpCommand();
    }

    public final void unregisterSubCommand(final CLCommand... subCommands) {
        for (final CLCommand subCommand : subCommands) {
            this.subCommands.remove(subCommand.getName());
            subCommand.setSuperCommand(null);
        }
        this.regenerateHelpCommand();
    }

    public final ImmutableList<CLCommand> getSubCommands() {
        return (ImmutableList<CLCommand>) ImmutableList.copyOf((Collection) this.subCommands.values());
    }

    private void regenerateHelpCommand() {
        if (!this.shouldGenerateHelpCommand()) {
            return;
        }
        final Map<String, CLCommand> subCommandsLV = this.subCommands;
        this.subCommands.put("help", new CLCommand("help") {
            public void handleCommandUnspecific(final CommandSender sender, final String[] args) {
                final StringBuilder builder = new StringBuilder();
                for (final Map.Entry<String, CLCommand> stringCLCommandEntry : subCommandsLV.entrySet()) {
                    builder.append(stringCLCommandEntry.getKey()).append("|");
                }
                final String s = builder.toString();
                sender.sendMessage(ChatColor.AQUA + "/" + ChatColor.DARK_AQUA + CLCommand.this.getFormattedName() + ChatColor.YELLOW + " - [" + s.substring(0, s.length() - 1) + "]");
            }
        });
    }

    public final boolean onCommand(final CommandSender sender, final Command command, final String s, final String[] args) {
        try {
            CLCommand subCommand = null;
            if (this.getClass().isAnnotationPresent(CommandPermission.class)) {
                final CommandPermission annotation = this.getClass().getAnnotation(CommandPermission.class);
                if (!sender.hasPermission(annotation.value()) && (!sender.isOp() || !annotation.isOpExempt())) {
                    throw new PermissionException("You do not have permission for this command!");
                }
            }
            if (this.isUsingSubCommandsOnly()) {
                if (args.length < 1) {
                    throw new ArgumentRequirementException("You must specify a sub-command for this command!");
                }
                if ((subCommand = this.getSubCommandFor(args[0])) == null) {
                    throw new ArgumentRequirementException("The sub-command you have specified is invalid!");
                }
            }
            if (subCommand == null && args.length > 0) {
                subCommand = this.getSubCommandFor(args[0]);
            }
            if (subCommand != null) {
                final String[] choppedArgs = (args.length < 2) ? new String[0] : Arrays.copyOfRange(args, 1, args.length);
                this.preSubCommandDispatch(sender, choppedArgs, subCommand);
                subCommand.onCommand(sender, command, s, choppedArgs);
                try {
                    this.handlePostSubCommand(sender, args);
                } catch (EmptyHandlerException ex2) {
                }
                return true;
            }
            try {
                if (sender instanceof Player) {
                    this.handleCommand((Player) sender, args);
                } else if (sender instanceof ConsoleCommandSender) {
                    this.handleCommand((ConsoleCommandSender) sender, args);
                } else if (sender instanceof BlockCommandSender) {
                    this.handleCommand((BlockCommandSender) sender, args);
                }
            } catch (EmptyHandlerException e2) {
                this.handleCommandUnspecific(sender, args);
            }
        } catch (CommandException ex) {
            this.handleCommandException(ex, args, sender);
        } catch (Exception e) {
            this.handleCommandException(new UnhandledCommandExceptionException(e), args, sender);
        }
        return true;
    }

    public final List<String> onTabComplete(final CommandSender sender, final Command command, final String alias, final String[] args) {
        if (this.getClass().isAnnotationPresent(CommandPermission.class)) {
            final CommandPermission annotation = this.getClass().getAnnotation(CommandPermission.class);
            if (!sender.hasPermission(annotation.value()) && (!sender.isOp() || !annotation.isOpExempt())) {
                return Collections.emptyList();
            }
        }
        if (args.length > 1) {
            final CLCommand possibleHigherLevelSubCommand;
            if ((possibleHigherLevelSubCommand = this.getSubCommandFor(args[0])) != null) {
                return possibleHigherLevelSubCommand.onTabComplete(sender, command, alias, Arrays.copyOfRange(args, 1, args.length));
            }
        } else if (args.length == 1) {
            final List<CLCommand> subCommandsForPartial = this.getSubCommandsForPartial(args[0]);
            if (subCommandsForPartial.size() != 0) {
                final List<String> strings = new ArrayList<String>();
                for (final CLCommand CLCommand : subCommandsForPartial) {
                    strings.add(CLCommand.getName());
                }
                final List<String> strings2 = this.handleTabComplete(sender, command, alias, args);
                strings.addAll(strings2);
                return strings;
            }
        }
        return this.handleTabComplete(sender, command, alias, args);
    }

    protected void handleCommandException(final CommandException ex, final String[] args, final CommandSender sender) {
        if (ex instanceof FriendlyException) {
            sender.sendMessage(((FriendlyException) ex).getFriendlyMessage(this));
        } else {
            sender.sendMessage(ChatColor.RED + ex.getClass().getSimpleName() + ": " + ex.getMessage() + "!");
        }
        if (ex instanceof UnhandledCommandExceptionException) {
            ((UnhandledCommandExceptionException) ex).getCausingException().printStackTrace();
        }
    }

    protected void preSubCommandDispatch(final CommandSender sender, final String[] args, final CLCommand subCommand) {
    }

    public final CLCommand getSubCommandFor(final String s) {
        if (this.subCommands.containsKey(s)) {
            return this.subCommands.get(s);
        }
        for (final String s2 : this.subCommands.keySet()) {
            if (s2.equalsIgnoreCase(s)) {
                return this.subCommands.get(s2);
            }
        }
        return null;
    }

    public final List<CLCommand> getSubCommandsForPartial(final String s) {
        final List<CLCommand> commands = new ArrayList<CLCommand>();
        final CLCommand subCommand;
        if ((subCommand = this.getSubCommandFor(s)) != null) {
            commands.add(subCommand);
            return commands;
        }
        final String s2 = s.toUpperCase();
        for (final String s3 : this.subCommands.keySet()) {
            if (s3.toUpperCase().startsWith(s2)) {
                commands.add(this.subCommands.get(s3));
            }
        }
        return commands;
    }

    protected void handleCommand(final Player player, final String[] args) throws CommandException {
        throw new EmptyHandlerException();
    }

    protected void handleCommand(final ConsoleCommandSender commandSender, final String[] args) throws CommandException {
        throw new EmptyHandlerException();
    }

    protected void handleCommand(final BlockCommandSender commandSender, final String[] args) throws CommandException {
        throw new EmptyHandlerException();
    }

    protected void handleCommandUnspecific(final CommandSender sender, final String[] args) throws CommandException {
        throw new EmptyHandlerException();
    }

    protected void handlePostSubCommand(final CommandSender sender, final String[] args) throws CommandException {
        throw new EmptyHandlerException();
    }

    protected boolean shouldGenerateHelpCommand() {
        return true;
    }

    protected List<String> handleTabComplete(final CommandSender sender, final Command command, final String alias, final String[] args) {
        if (this.isUsingSubCommandsOnly() || this.subCommands.size() > 0) {
            return Collections.emptyList();
        }
        final List<String> ss = new ArrayList<String>();
        final String arg = (args.length > 0) ? args[args.length - 1].toLowerCase() : "";
        for (final Player player : Bukkit.getOnlinePlayers()) {
            final String name1 = player.getName();
            if (name1.toLowerCase().startsWith(arg)) {
                ss.add(name1);
            }
        }
        return ss;
    }

    protected boolean isUsingSubCommandsOnly() {
        return false;
    }

    protected String getFormattedName() {
        return (this.superCommand == null) ? this.name : (String.valueOf(this.superCommand.getFormattedName()) + " " + this.name);
    }

    @Override
    public String toString() {
        return "Command -> " + this.getFormattedName();
    }

    public void setMeta(final CommandMeta meta) {
        this.meta = meta;
    }

    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof CLCommand)) {
            return false;
        }
        final CLCommand other = (CLCommand) o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$subCommands = this.getSubCommands();
        final Object other$subCommands = other.getSubCommands();
        Label_0068:
        {
            if (this$subCommands == null) {
                if (other$subCommands == null) {
                    break Label_0068;
                }
            } else if (this$subCommands.equals(other$subCommands)) {
                break Label_0068;
            }
            return false;
        }
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        Label_0108:
        {
            if (this$name == null) {
                if (other$name == null) {
                    break Label_0108;
                }
            } else if (this$name.equals(other$name)) {
                break Label_0108;
            }
            return false;
        }
        final Object this$superCommand = this.getSuperCommand();
        final Object other$superCommand = other.getSuperCommand();
        Label_0148:
        {
            if (this$superCommand == null) {
                if (other$superCommand == null) {
                    break Label_0148;
                }
            } else if (this$superCommand.equals(other$superCommand)) {
                break Label_0148;
            }
            return false;
        }
        final Object this$meta = this.getMeta();
        final Object other$meta = other.getMeta();
        if (this$meta == null) {
            if (other$meta == null) {
                return true;
            }
        } else if (this$meta.equals(other$meta)) {
            return true;
        }
        return false;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof CLCommand;
    }

    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $subCommands = this.getSubCommands();
        result = result * 59 + (($subCommands == null) ? 0 : $subCommands.hashCode());
        final Object $name = this.getName();
        result = result * 59 + (($name == null) ? 0 : $name.hashCode());
        final Object $superCommand = this.getSuperCommand();
        result = result * 59 + (($superCommand == null) ? 0 : $superCommand.hashCode());
        final Object $meta = this.getMeta();
        result = result * 59 + (($meta == null) ? 0 : $meta.hashCode());
        return result;
    }

    public String getName() {
        return this.name;
    }

    protected void setSuperCommand(final CLCommand superCommand) {
        this.superCommand = superCommand;
    }

    public CLCommand getSuperCommand() {
        return this.superCommand;
    }

    public CommandMeta getMeta() {
        return this.meta;
    }
}
