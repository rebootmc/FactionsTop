package me.twister915.corelite.command;

public class UnhandledCommandExceptionException extends CommandException
{
    private final Exception causingException;
    
    public UnhandledCommandExceptionException(final Exception e) {
        super("Unhandled exception " + e.getMessage());
        this.causingException = e;
    }
    
    public Exception getCausingException() {
        return this.causingException;
    }
}
