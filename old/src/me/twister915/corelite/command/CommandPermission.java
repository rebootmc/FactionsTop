package me.twister915.corelite.command;

import java.lang.annotation.*;

@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface CommandPermission {
    String value();
    
    boolean isOpExempt() default true;
}
