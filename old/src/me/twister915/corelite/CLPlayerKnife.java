package me.twister915.corelite;

import org.bukkit.entity.*;
import org.bukkit.inventory.*;
import org.bukkit.*;
import org.bukkit.potion.*;
import java.util.*;

public final class CLPlayerKnife
{
    private final Player player;
    
    public static CLPlayerKnife $(final Player player) {
        return new CLPlayerKnife(player);
    }
    
    public CLPlayerKnife playSoundForPlayer(final Sound sound) {
        this.player.playSound(this.player.getLocation(), sound, 20.0f, 0.0f);
        return this;
    }
    
    public CLPlayerKnife resetPlayer() {
        this.player.setMaxHealth(20.0);
        this.player.setHealth(this.player.getMaxHealth());
        this.player.setFireTicks(0);
        this.player.setFoodLevel(20);
        this.player.resetPlayerTime();
        this.player.resetPlayerWeather();
        this.player.getInventory().clear();
        this.player.getInventory().setArmorContents(new ItemStack[4]);
        this.player.setExp(0.0f);
        this.player.setLevel(0);
        if (this.player.getAllowFlight()) {
            this.player.setFlying(false);
        }
        this.player.setAllowFlight(false);
        this.player.setGameMode(GameMode.SURVIVAL);
        for (final PotionEffect potionEffect : this.player.getActivePotionEffects()) {
            this.player.removePotionEffect(potionEffect.getType());
        }
        return this;
    }
    
    public CLPlayerKnife(final Player player) {
        this.player = player;
    }
    
    public Player getPlayer() {
        return this.player;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof CLPlayerKnife)) {
            return false;
        }
        final CLPlayerKnife other = (CLPlayerKnife)o;
        final Object this$player = this.getPlayer();
        final Object other$player = other.getPlayer();
        if (this$player == null) {
            if (other$player == null) {
                return true;
            }
        }
        else if (this$player.equals(other$player)) {
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $player = this.getPlayer();
        result = result * 59 + (($player == null) ? 0 : $player.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "CLPlayerKnife(player=" + this.getPlayer() + ")";
    }
}
