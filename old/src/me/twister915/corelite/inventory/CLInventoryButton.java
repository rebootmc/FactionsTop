package me.twister915.corelite.inventory;

import org.bukkit.inventory.*;
import lombok.*;
import org.bukkit.inventory.meta.*;
import org.bukkit.*;
import java.util.*;
import org.bukkit.entity.*;
import me.twister915.corelite.command.*;

public abstract class CLInventoryButton
{
    private static final Integer LORE_LINE_LENGTH;
    @NonNull
    private ItemStack stack;
    
    static {
        LORE_LINE_LENGTH = 30;
    }
    
    protected CLInventoryButton(final ItemStack stack) {
        this.stack = stack;
    }
    
    protected CLInventoryButton(final Material material, final String title, final String lore) {
        this.setItemStackUsing(material, 1, title, lore);
    }
    
    protected final void setItemStackUsing(final Material material, final Integer quantity, final String title, final String lore) {
        this.stack = new ItemStack(material, (int)quantity);
        final ItemMeta meta = this.stack.getItemMeta();
        meta.setDisplayName(title);
        meta.setLore((List)wrapLoreText(lore));
        this.stack.setItemMeta(meta);
    }
    
    public static List<String> wrapLoreText(final String string) {
        final String workingString = ChatColor.translateAlternateColorCodes('&', string).trim();
        if (workingString.length() <= CLInventoryButton.LORE_LINE_LENGTH) {
            return Arrays.asList(workingString);
        }
        final double numberOfLines = Math.ceil(workingString.length() / CLInventoryButton.LORE_LINE_LENGTH);
        final List<String> lines = new ArrayList<String>();
        String lastColor = null;
        for (int lineIndex = 0; lineIndex < numberOfLines; ++lineIndex) {
            String line = workingString.substring(lineIndex * CLInventoryButton.LORE_LINE_LENGTH, Math.min((lineIndex + 1) * CLInventoryButton.LORE_LINE_LENGTH, workingString.length()));
            if (lastColor != null) {
                line = String.valueOf(lastColor) + line;
            }
            lastColor = ChatColor.getLastColors(line);
            lines.add(line);
        }
        return lines;
    }
    
    protected void onPlayerClick(final Player player, final ClickAction action) throws EmptyHandlerException {
        throw new EmptyHandlerException();
    }
    
    @NonNull
    public ItemStack getStack() {
        return this.stack;
    }
    
    @Override
    public String toString() {
        return "CLInventoryButton(stack=" + this.getStack() + ")";
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof CLInventoryButton)) {
            return false;
        }
        final CLInventoryButton other = (CLInventoryButton)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$stack = this.stack;
        final Object other$stack = other.stack;
        if (this$stack == null) {
            if (other$stack == null) {
                return true;
            }
        }
        else if (this$stack.equals(other$stack)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof CLInventoryButton;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $stack = this.stack;
        result = result * 59 + (($stack == null) ? 0 : $stack.hashCode());
        return result;
    }
    
    protected void setStack(@NonNull final ItemStack stack) {
        if (stack == null) {
            throw new NullPointerException("stack");
        }
        this.stack = stack;
    }
}
