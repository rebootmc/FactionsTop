package me.twister915.corelite.inventory;

import org.bukkit.entity.*;
import org.bukkit.inventory.*;

public abstract class ControlledInventoryButton
{
    protected void onUse(final Player player) {
    }
    
    protected abstract ItemStack getStack(final Player p0);
}
