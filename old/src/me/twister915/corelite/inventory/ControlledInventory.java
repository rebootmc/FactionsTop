package me.twister915.corelite.inventory;

import org.bukkit.entity.*;
import java.util.*;
import org.bukkit.inventory.*;
import org.bukkit.event.inventory.*;
import org.bukkit.event.*;
import org.bukkit.*;
import org.bukkit.event.block.*;
import org.bukkit.event.player.*;

public abstract class ControlledInventory implements Listener
{
    private final Map<Integer, ControlledInventoryButton> buttons;
    private final Set<Player> players;
    
    public ControlledInventory() {
        this(true);
    }
    
    public ControlledInventory(final boolean reload) {
        this.buttons = new HashMap<Integer, ControlledInventoryButton>();
        this.players = new HashSet<Player>();
        if (reload) {
            this.reload();
        }
    }
    
    protected abstract ControlledInventoryButton getNewButtonAt(final Integer p0);
    
    public final void reload() {
        this.buttons.clear();
        for (int i = 0; i < 36; ++i) {
            final ControlledInventoryButton newButtonAt = this.getNewButtonAt(i);
            if (newButtonAt != null) {
                this.buttons.put(i, newButtonAt);
            }
        }
        for (final Player player : this.players) {
            this.updateForPlayer(player);
        }
    }
    
    public final void updateItems() {
        for (final Player player : this.players) {
            this.updateForPlayer(player);
        }
    }
    
    public final void setActive(final Player player) {
        this.players.add(player);
        this.updateForPlayer(player);
    }
    
    public final void remove(final Player player) {
        if (!this.players.contains(player)) {
            return;
        }
        this.clearForPlayer(player);
        this.players.remove(player);
    }
    
    protected void updateForPlayer(final Player player) {
        for (final Map.Entry<Integer, ControlledInventoryButton> entry : this.buttons.entrySet()) {
            player.getInventory().setItem((int)entry.getKey(), entry.getValue().getStack(player));
        }
        player.updateInventory();
    }
    
    private void clearForPlayer(final Player player) {
        for (final Integer integer : this.buttons.keySet()) {
            player.getInventory().setItem((int)integer, (ItemStack)null);
        }
        player.updateInventory();
    }
    
    @EventHandler
    public final void onPlayerQuit(final PlayerQuitEvent event) {
        this.players.remove(event.getPlayer());
    }
    
    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
    public final void onPlayerInventoryMove(final InventoryClickEvent event) {
        if (!(event.getWhoClicked() instanceof Player)) {
            return;
        }
        final Player onlinePlayer = (Player)event.getWhoClicked();
        if (!this.players.contains(onlinePlayer)) {
            return;
        }
        if (this.buttons.keySet().contains(event.getSlot()) && this.players.contains(onlinePlayer) && event.getInventory().equals(onlinePlayer.getInventory())) {
            event.setCancelled(true);
        }
    }
    
    @EventHandler(priority = EventPriority.HIGH)
    public final void onInteract(final PlayerInteractEvent event) {
        if (event.getPlayer().getGameMode() == GameMode.CREATIVE) {
            return;
        }
        if (event.getAction() == Action.PHYSICAL) {
            return;
        }
        final ControlledInventoryButton controlledInventoryButton = this.buttons.get(event.getPlayer().getInventory().getHeldItemSlot());
        if (controlledInventoryButton == null) {
            return;
        }
        final Player onlinePlayer = event.getPlayer();
        if (!this.players.contains(onlinePlayer)) {
            return;
        }
        controlledInventoryButton.onUse(onlinePlayer);
        this.updateForPlayer(onlinePlayer);
        event.setCancelled(true);
    }
    
    @EventHandler(priority = EventPriority.LOW)
    public final void onPlayerDrop(final PlayerDropItemEvent event) {
        final Player onlinePlayer = event.getPlayer();
        if (!this.players.contains(onlinePlayer)) {
            return;
        }
        if (this.buttons.get(onlinePlayer.getInventory().getHeldItemSlot()) != null) {
            event.setCancelled(false);
            event.getItemDrop().remove();
            this.updateForPlayer(onlinePlayer);
        }
    }
    
    protected final Set<Map.Entry<Integer, ControlledInventoryButton>> getButtons() {
        return this.buttons.entrySet();
    }
    
    public Set<Player> getPlayers() {
        return this.players;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ControlledInventory)) {
            return false;
        }
        final ControlledInventory other = (ControlledInventory)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$buttons = this.getButtons();
        final Object other$buttons = other.getButtons();
        Label_0068: {
            if (this$buttons == null) {
                if (other$buttons == null) {
                    break Label_0068;
                }
            }
            else if (this$buttons.equals(other$buttons)) {
                break Label_0068;
            }
            return false;
        }
        final Object this$players = this.getPlayers();
        final Object other$players = other.getPlayers();
        if (this$players == null) {
            if (other$players == null) {
                return true;
            }
        }
        else if (this$players.equals(other$players)) {
            return true;
        }
        return false;
    }
    
    protected boolean canEqual(final Object other) {
        return other instanceof ControlledInventory;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $buttons = this.getButtons();
        result = result * 59 + (($buttons == null) ? 0 : $buttons.hashCode());
        final Object $players = this.getPlayers();
        result = result * 59 + (($players == null) ? 0 : $players.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "ControlledInventory(buttons=" + this.getButtons() + ", players=" + this.getPlayers() + ")";
    }
}
