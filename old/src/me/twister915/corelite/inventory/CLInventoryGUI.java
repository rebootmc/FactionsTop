package me.twister915.corelite.inventory;

import org.bukkit.entity.*;
import me.twister915.corelite.plugin.*;
import com.google.common.collect.*;
import java.util.*;
import org.bukkit.inventory.*;
import org.bukkit.event.player.*;
import org.bukkit.event.*;
import me.twister915.corelite.*;
import org.bukkit.*;
import me.twister915.corelite.command.*;
import org.bukkit.event.inventory.*;

public class CLInventoryGUI implements Listener
{
    protected final List<Player> observers;
    protected final String title;
    protected Inventory inventory;
    protected final Map<Integer, CLInventoryButton> inventoryButtons;
    protected Set<Integer> updatedSlots;
    private final CLPlugin plugin;
    private boolean registered;
    
    public CLInventoryGUI(final Integer size, final String title, final CLPlugin plugin) {
        this.observers = new LinkedList<Player>();
        this.inventoryButtons = new HashMap<Integer, CLInventoryButton>();
        this.updatedSlots = new HashSet<Integer>();
        this.registered = false;
        if (size % 9 != 0) {
            throw new IllegalArgumentException("The size of an inventory must be divisible by 9 evenly.");
        }
        this.title = title;
        this.inventory = Bukkit.createInventory((InventoryHolder)null, (int)size, title.substring(0, Math.min(32, title.length())));
        this.plugin = plugin;
    }
    
    public void open(final Player player) {
        if (this.observers.contains(player)) {
            return;
        }
        this.observers.add(player);
        if (this.observers.size() == 1 && !this.registered) {
            this.plugin.registerListener(this);
        }
        this.registered = true;
        player.openInventory(this.inventory);
    }
    
    public void close(final Player player) {
        if (!this.observers.contains(player)) {
            return;
        }
        this.observers.remove(player);
        if (this.observers.size() == 0 && this.registered) {
            HandlerList.unregisterAll((Listener)this);
            this.registered = false;
        }
        player.closeInventory();
        this.onClose(player);
    }
    
    public void open(final Iterable<Player> players) {
        for (final Player player : players) {
            this.open(player);
        }
    }
    
    public void close(final Iterable<Player> players) {
        for (final Player player : players) {
            this.close(player);
        }
    }
    
    public ImmutableList<Player> getCurrentObservers() {
        return (ImmutableList<Player>)ImmutableList.copyOf((Collection)this.observers);
    }
    
    public void addButton(final CLInventoryButton button) {
        final Integer nextOpenSlot = this.getNextOpenSlot();
        if (nextOpenSlot == null) {
            throw new IllegalStateException("Unable to place the button in the inventory, no room remains!");
        }
        this.addButton(button, nextOpenSlot);
    }
    
    public void addButton(final CLInventoryButton button, final Integer slot) {
        this.inventoryButtons.put(slot, button);
        this.markForUpdate(slot);
    }
    
    public void moveButton(final CLInventoryButton button, final Integer slot) {
        this.removeButton(button);
        this.addButton(button, slot);
    }
    
    public void markForUpdate(final CLInventoryButton button) {
        this.markForUpdate(this.getSlotFor(button));
    }
    
    public void markForUpdate(final Integer slot) {
        this.updatedSlots.add(slot);
    }
    
    public Integer getSlotFor(final CLInventoryButton button) {
        for (final Map.Entry<Integer, CLInventoryButton> integerInventoryButtonEntry : this.inventoryButtons.entrySet()) {
            if (integerInventoryButtonEntry.getValue().equals(button)) {
                return integerInventoryButtonEntry.getKey();
            }
        }
        return -1;
    }
    
    public void removeButton(final CLInventoryButton button) {
        this.clearSlot(this.getSlotFor(button));
    }
    
    public void clearSlot(final Integer slot) {
        this.inventoryButtons.remove(slot);
        this.markForUpdate(slot);
    }
    
    public void onClose(final Player onlinePlayer) {
    }
    
    public boolean isFilled(final Integer slot) {
        return this.inventoryButtons.containsKey(slot);
    }
    
    public void updateInventory() {
        for (int x = 0; x < this.inventory.getSize(); ++x) {
            final CLInventoryButton inventoryButton = this.inventoryButtons.get(x);
            if (inventoryButton == null && this.inventory.getItem(x) != null) {
                this.inventory.setItem(x, (ItemStack)null);
            }
            else if ((this.inventory.getItem(x) == null && inventoryButton != null) || this.updatedSlots.contains(x)) {
                assert inventoryButton != null;
                this.inventory.setItem(x, inventoryButton.getStack());
            }
        }
        for (final Player observer : this.observers) {
            observer.updateInventory();
        }
        this.updatedSlots = new HashSet<Integer>();
    }
    
    private Integer getNextOpenSlot() {
        Integer nextSlot = 0;
        for (final Integer integer : this.inventoryButtons.keySet()) {
            if (integer.equals(nextSlot)) {
                nextSlot = integer + 1;
            }
        }
        return (nextSlot >= this.inventory.getSize()) ? null : nextSlot;
    }
    
    @EventHandler(priority = EventPriority.HIGH)
    public final void onPlayerLeave(final PlayerQuitEvent event) {
        final Player onlinePlayer = event.getPlayer();
        if (this.observers.contains(onlinePlayer)) {
            this.observers.remove(onlinePlayer);
        }
    }
    
    @EventHandler
    public final void onInventoryClose(final InventoryCloseEvent event) {
        if (!(event.getPlayer() instanceof Player)) {
            return;
        }
        if (!event.getInventory().equals(this.inventory)) {
            return;
        }
        final Player player = (Player)event.getPlayer();
        this.observers.remove(player);
        this.onClose(player);
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public final void onInventoryClick(final InventoryClickEvent event) {
        if (!(event.getWhoClicked() instanceof Player)) {
            return;
        }
        if (!event.getInventory().equals(this.inventory)) {
            return;
        }
        final Player player = (Player)event.getWhoClicked();
        final CLInventoryButton CLInventoryButton = this.inventoryButtons.get(event.getSlot());
        if (player == null) {
            throw new IllegalStateException("Somehow, someone who was null clicked on a slot that was null or had no button...");
        }
        if (CLInventoryButton == null) {
            return;
        }
        try {
            CLInventoryButton.onPlayerClick(player, getActionTypeFor(event.getClick()));
        }
        catch (EmptyHandlerException e) {
            CLPlayerKnife.$(player).playSoundForPlayer(Sound.NOTE_PLING);
        }
        event.setCancelled(true);
    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public final void onPlayerInventoryMove(final InventoryMoveItemEvent event) {
        if (!event.getDestination().equals(this.inventory)) {
            return;
        }
        event.setCancelled(true);
    }
    
    public ImmutableList<CLInventoryButton> getButtons() {
        return (ImmutableList<CLInventoryButton>)ImmutableList.copyOf((Collection)this.inventoryButtons.values());
    }
    
    private static ClickAction getActionTypeFor(final ClickType click) {
        switch (click) {
            case RIGHT:
            case SHIFT_RIGHT: {
                return ClickAction.RIGHT_CLICK;
            }
            default: {
                return ClickAction.LEFT_CLICK;
            }
        }
    }
}
