package me.twister915.corelite.util;

import java.nio.file.*;
import java.io.*;

public final class CLUtil
{
    public static boolean delete(final File file) {
        if (file.isDirectory()) {
            final File[] files = file.listFiles();
            if (files == null) {
                return false;
            }
            File[] array;
            for (int length = (array = files).length, i = 0; i < length; ++i) {
                final File file2 = array[i];
                if (!delete(file2)) {
                    return false;
                }
            }
        }
        return file.delete();
    }
    
    public static void copy(final File source, final File dest) throws IOException {
        Files.copy(source.toPath(), dest.toPath(), new CopyOption[0]);
        if (source.isDirectory()) {
            String[] list;
            for (int length = (list = source.list()).length, i = 0; i < length; ++i) {
                final String s = list[i];
                copy(new File(source, s), new File(dest, s));
            }
        }
    }
    
    public static <T> boolean arrayContains(final T[] ts, final T t) {
        for (final T t2 : ts) {
            if (((t2 != null && t != null) || t == t2) && (t2 == t || t2.equals(t))) {
                return true;
            }
        }
        return false;
    }
    
    public static String formatSeconds(final Integer seconds) {
        final StringBuilder builder = new StringBuilder();
        int ofNext = seconds;
        TimeUnit[] values;
        for (int length = (values = TimeUnit.values()).length, i = 0; i < length; ++i) {
            final TimeUnit unit = values[i];
            int ofUnit;
            if (unit.perNext != -1) {
                ofUnit = ofNext % unit.perNext;
                ofNext = Math.floorDiv(ofNext, unit.perNext);
            }
            else {
                ofUnit = ofNext;
                ofNext = 0;
            }
            builder.insert(0, unit.shortName).insert(0, String.format("%02d", ofUnit));
            if (ofNext == 0) {
                break;
            }
        }
        return builder.toString();
    }
    
    private enum TimeUnit
    {
        SECONDS("SECONDS", 0, 60, 's'), 
        MINUTES("MINUTES", 1, 60, 'm'), 
        HOURS("HOURS", 2, 24, 'h'), 
        DAYS("DAYS", 3, 'd');
        
        private final int perNext;
        private final char shortName;
        
        private TimeUnit(final String s, final int n, final int i, final char h) {
            this.perNext = i;
            this.shortName = h;
        }
        
        private TimeUnit(final String s, final int n, final char d) {
            this.perNext = -1;
            this.shortName = d;
        }
    }
}
