package me.twister915.corelite.util;

import org.bukkit.*;
import org.bukkit.block.*;
import org.bukkit.entity.*;

public final class Point
{
    private final double x;
    private final double y;
    private final double z;
    private final float pitch;
    private final float yaw;
    
    public Location in(final World world) {
        return new Location(world, this.x, this.y, this.z, this.yaw, this.pitch);
    }
    
    public double distanceSquared(final Point point) {
        return Math.pow(this.x - point.getX(), 2.0) + Math.pow(this.y - point.getY(), 2.0) + Math.pow(this.z - point.getZ(), 2.0);
    }
    
    public double distance(final Point point) {
        return Math.sqrt(this.distanceSquared(point));
    }
    
    public static Point of(final Location location) {
        return new Point(location.getX(), location.getY(), location.getZ(), location.getPitch(), location.getYaw());
    }
    
    public static Point of(final Block block) {
        return new Point(block.getX(), block.getY(), block.getZ(), 0.0f, 0.0f);
    }
    
    public static Point of(final Entity entity) {
        return of(entity.getLocation());
    }
    
    public Point add(final double x, final double y, final double z) {
        return new Point(this.x + x, this.y + y, this.z + z, this.pitch, this.yaw);
    }
    
    public Point subtract(final double x, final double y, final double z) {
        return this.add(-x, -y, -z);
    }
    
    public Point(final double x, final double y, final double z, final float pitch, final float yaw) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.pitch = pitch;
        this.yaw = yaw;
    }
    
    public double getX() {
        return this.x;
    }
    
    public double getY() {
        return this.y;
    }
    
    public double getZ() {
        return this.z;
    }
    
    public float getPitch() {
        return this.pitch;
    }
    
    public float getYaw() {
        return this.yaw;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Point)) {
            return false;
        }
        final Point other = (Point)o;
        return Double.compare(this.getX(), other.getX()) == 0 && Double.compare(this.getY(), other.getY()) == 0 && Double.compare(this.getZ(), other.getZ()) == 0 && Float.compare(this.getPitch(), other.getPitch()) == 0 && Float.compare(this.getYaw(), other.getYaw()) == 0;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final long $x = Double.doubleToLongBits(this.getX());
        result = result * 59 + (int)($x >>> 32 ^ $x);
        final long $y = Double.doubleToLongBits(this.getY());
        result = result * 59 + (int)($y >>> 32 ^ $y);
        final long $z = Double.doubleToLongBits(this.getZ());
        result = result * 59 + (int)($z >>> 32 ^ $z);
        result = result * 59 + Float.floatToIntBits(this.getPitch());
        result = result * 59 + Float.floatToIntBits(this.getYaw());
        return result;
    }
    
    @Override
    public String toString() {
        return "Point(x=" + this.getX() + ", y=" + this.getY() + ", z=" + this.getZ() + ", pitch=" + this.getPitch() + ", yaw=" + this.getYaw() + ")";
    }
}
