package me.twister915.corelite.util;

public final class Region
{
    private Point min;
    private Point max;
    
    public Region(final Point a, final Point b) {
        this.setMinMax(a, b);
    }
    
    public void setMin(final Point point) {
        this.setMinMax(this.max, point);
    }
    
    public void setMax(final Point point) {
        this.setMinMax(this.min, point);
    }
    
    public void setMinMax(final Point a, final Point b) {
        this.min = new Point(Math.min(a.getX(), b.getX()), Math.min(a.getY(), b.getY()), Math.min(a.getZ(), b.getZ()), Math.min(a.getPitch(), b.getPitch()), Math.min(a.getYaw(), b.getYaw()));
        this.max = new Point(Math.max(a.getX(), b.getX()), Math.max(a.getY(), b.getY()), Math.max(a.getZ(), b.getZ()), Math.max(a.getPitch(), b.getPitch()), Math.max(a.getYaw(), b.getYaw()));
    }
    
    public boolean inRegion(final Point point) {
        return point.getX() >= this.min.getX() && point.getX() <= this.max.getX() && point.getY() >= this.min.getY() && point.getY() <= this.max.getY() && point.getZ() >= this.min.getZ() && point.getZ() <= this.max.getZ();
    }
    
    public Point getMin() {
        return this.min;
    }
    
    public Point getMax() {
        return this.max;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Region)) {
            return false;
        }
        final Region other = (Region)o;
        final Object this$min = this.getMin();
        final Object other$min = other.getMin();
        Label_0058: {
            if (this$min == null) {
                if (other$min == null) {
                    break Label_0058;
                }
            }
            else if (this$min.equals(other$min)) {
                break Label_0058;
            }
            return false;
        }
        final Object this$max = this.getMax();
        final Object other$max = other.getMax();
        if (this$max == null) {
            if (other$max == null) {
                return true;
            }
        }
        else if (this$max.equals(other$max)) {
            return true;
        }
        return false;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $min = this.getMin();
        result = result * 59 + (($min == null) ? 0 : $min.hashCode());
        final Object $max = this.getMax();
        result = result * 59 + (($max == null) ? 0 : $max.hashCode());
        return result;
    }
    
    @Override
    public String toString() {
        return "Region(min=" + this.getMin() + ", max=" + this.getMax() + ")";
    }
}
