package me.twister915.corelite.util;

import org.bukkit.enchantments.*;
import org.bukkit.*;
import java.util.*;
import org.bukkit.inventory.*;
import org.bukkit.inventory.meta.*;

public final class ItemShorthand
{
    private final Material material;
    private String name;
    private List<String> lore;
    private Map<Enchantment, Integer> enchantments;
    private short dataValue;
    private int quantity;
    
    public ItemShorthand(final Material m) {
        this.material = m;
    }
    
    public static ItemShorthand setMaterial(final Material material) {
        return new ItemShorthand(material);
    }
    
    public ItemShorthand setQuantity(final int quantity) {
        this.quantity = quantity;
        return this;
    }
    
    public ItemShorthand setName(final String name) {
        this.name = name;
        return this;
    }
    
    public ItemShorthand setLore(final String l) {
        this.checkLore();
        this.lore.add(ChatColor.translateAlternateColorCodes('&', l));
        return this;
    }
    
    public ItemShorthand setDataValue(final short dataValue) {
        this.dataValue = dataValue;
        return this;
    }
    
    public ItemShorthand setEnchantment(final Enchantment enchantment, final int level) {
        this.checkEnchantments();
        this.enchantments.put(enchantment, level);
        return this;
    }
    
    private void checkLore() {
        if (this.lore == null) {
            this.lore = new ArrayList<String>();
        }
    }
    
    private void checkEnchantments() {
        if (this.enchantments == null) {
            this.enchantments = new HashMap<Enchantment, Integer>();
        }
    }
    
    public ItemStack get() {
        final ItemStack item = new ItemStack(this.material);
        final ItemMeta meta = item.getItemMeta();
        if (this.name != null) {
            meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', this.name));
        }
        if (this.lore != null) {
            meta.setLore((List)this.lore);
        }
        item.setItemMeta(meta);
        if (this.quantity > 1) {
            item.setAmount(this.quantity);
        }
        if (this.dataValue > 0) {
            item.setDurability(this.dataValue);
        }
        if (this.enchantments != null) {
            item.addUnsafeEnchantments((Map)this.enchantments);
        }
        return item;
    }
}
