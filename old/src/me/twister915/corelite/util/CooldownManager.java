package me.twister915.corelite.util;

import java.util.*;
import java.util.concurrent.*;

public final class CooldownManager
{
    private final Map<String, Date> cooldownMilliseconds;
    
    public CooldownManager() {
        this.cooldownMilliseconds = new HashMap<String, Date>();
    }
    
    public void testCooldown(final String key, final Long time, final TimeUnit unit, final Boolean reset) throws CooldownUnexpiredException {
        final Date lastFiredDate = this.cooldownMilliseconds.get(key);
        final Date currentDate = new Date();
        if (lastFiredDate == null) {
            this.cooldownMilliseconds.put(key, currentDate);
            return;
        }
        final long millisecondsPassed = currentDate.getTime() - lastFiredDate.getTime();
        final long milliseconds = unit.toMillis(time);
        if (milliseconds >= millisecondsPassed) {
            if (reset) {
                this.cooldownMilliseconds.put(key, currentDate);
            }
            throw new CooldownUnexpiredException(Long.valueOf(unit.toMillis(milliseconds - millisecondsPassed)), unit);
        }
        this.cooldownMilliseconds.put(key, currentDate);
    }
    
    public void testCooldown(final String key, final Long time, final TimeUnit unit) throws CooldownUnexpiredException {
        this.testCooldown(key, time, unit, false);
    }
    
    public void testCooldown(final String key, final Long seconds) throws CooldownUnexpiredException {
        this.testCooldown(key, seconds, TimeUnit.SECONDS);
    }
}
