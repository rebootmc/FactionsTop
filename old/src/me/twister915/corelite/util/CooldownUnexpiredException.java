package me.twister915.corelite.util;

import me.twister915.corelite.command.*;
import java.util.concurrent.*;

public final class CooldownUnexpiredException extends CommandException
{
    private final Long timeRemaining;
    private final TimeUnit timeUnit;
    
    public CooldownUnexpiredException(final Long timeRemaining, final TimeUnit timeUnit) {
        super("Unexpired cooldown");
        this.timeRemaining = timeRemaining;
        this.timeUnit = timeUnit;
    }
    
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
    
    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof CooldownUnexpiredException)) {
            return false;
        }
        final CooldownUnexpiredException other = (CooldownUnexpiredException)o;
        if (!other.canEqual(this)) {
            return false;
        }
        final Object this$timeRemaining = this.getTimeRemaining();
        final Object other$timeRemaining = other.getTimeRemaining();
        Label_0068: {
            if (this$timeRemaining == null) {
                if (other$timeRemaining == null) {
                    break Label_0068;
                }
            }
            else if (this$timeRemaining.equals(other$timeRemaining)) {
                break Label_0068;
            }
            return false;
        }
        final Object this$timeUnit = this.getTimeUnit();
        final Object other$timeUnit = other.getTimeUnit();
        if (this$timeUnit == null) {
            if (other$timeUnit == null) {
                return true;
            }
        }
        else if (this$timeUnit.equals(other$timeUnit)) {
            return true;
        }
        return false;
    }
    
    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof CooldownUnexpiredException;
    }
    
    @Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $timeRemaining = this.getTimeRemaining();
        result = result * 59 + (($timeRemaining == null) ? 0 : $timeRemaining.hashCode());
        final Object $timeUnit = this.getTimeUnit();
        result = result * 59 + (($timeUnit == null) ? 0 : $timeUnit.hashCode());
        return result;
    }
    
    public Long getTimeRemaining() {
        return this.timeRemaining;
    }
    
    public TimeUnit getTimeUnit() {
        return this.timeUnit;
    }
    
    @Override
    public String toString() {
        return "CooldownUnexpiredException(timeRemaining=" + this.getTimeRemaining() + ", timeUnit=" + this.getTimeUnit() + ")";
    }
}
