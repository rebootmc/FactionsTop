package xyz.boomclaw.plugins.factionstop.versions.one_eight;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.Factions;
import xyz.boomclaw.plugins.factionstop.FactionsTop;
import xyz.boomclaw.plugins.factionstop.models.LTResult;

public class BalancesRunnable implements Runnable
{

	private FactionsTop plugin;
	private LTResult    result;

	public BalancesRunnable(FactionsTop plugin, LTResult result)
	{
		this.plugin = plugin;
		this.result = result;
	}

	@Override
	public void run()
	{
		int    balance    = 0;
		double currentMax = Integer.MIN_VALUE;
		String richest    = "";

		if (plugin.getEconomy() != null)
		{
			if (Factions.i.get(result.getId()) == null)
			{
				return;
			}

			for (FPlayer player : Factions.i.get(result.getId()).getFPlayers())
			{
				double balanceTemp = plugin.getEconomy().getBalance(player.getName());

				if (balanceTemp > currentMax)
				{
					richest = player.getName();
					currentMax = balanceTemp;
				}

				balance += balanceTemp;
			}
		}

		result.setRichest(richest);
		result.setBalances(balance);
	}

}