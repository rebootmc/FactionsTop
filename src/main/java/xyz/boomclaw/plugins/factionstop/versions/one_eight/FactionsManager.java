package xyz.boomclaw.plugins.factionstop.versions.one_eight;

import com.massivecraft.factions.*;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.ChunkSnapshot;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import xyz.boomclaw.plugins.factionstop.FactionsTop;
import xyz.boomclaw.plugins.factionstop.models.ChunkResult;
import xyz.boomclaw.plugins.factionstop.models.FactionsResult;
import xyz.boomclaw.plugins.factionstop.models.LTResult;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class FactionsManager extends xyz.boomclaw.plugins.factionstop.manager.FactionsManager
{

	private List<Faction> factionsToCheck = new ArrayList<>();

	public FactionsManager(FactionsTop plugin)
	{
		super(plugin);

		for (Faction faction : Factions.i.get())
		{
			if (!faction.isNormal() && faction.isNone())
			{
				continue;
			}

			if (faction.getTag().equalsIgnoreCase("WarZone") ||
					faction.getTag().equalsIgnoreCase("SafeZone"))
			{
				continue;
			}

			factionsToCheck.add(faction);
		}

		Bukkit.getScheduler().runTaskTimer(plugin, new Runnable()
		{
			List<Faction> factionsToReCheck = new ArrayList<>();

			@Override
			public void run()
			{
				int x = 0;

				if (factionsToReCheck.isEmpty())
				{
					factionsToReCheck.addAll(Factions.i.get());
				}

				Iterator iterator = factionsToReCheck.iterator();

				while (iterator.hasNext())
				{
					if (x >= 5)
					{
						break;
					}

					x++;

					Faction faction = (Faction) iterator.next();

					LTResult tResult = null;

					for (LTResult result : results)
					{
						if (result.getId().equals(faction.getId()))
						{
							tResult = result;
						}
					}

					if (tResult == null)
					{
						if (!factionsToCheck.contains(faction))
						{
							factionsToCheck.add(faction);
						}

						iterator.remove();
						continue;
					}

					for (FLocation fLocation : Board.getFactionClaims(faction))
					{
						boolean scanned = false;

						for (ChunkResult cResult : tResult.getChunkResults())
						{
							if (cResult.getX() == fLocation.getX() &&
									cResult.getZ() == fLocation.getZ())
							{
								scanned = true;
							}
						}

						if (!scanned)
						{
							scanChunk(faction.getId(), Bukkit.getWorld(fLocation.getWorldName()).getChunkAt((int) fLocation.getX(), (int) fLocation.getZ()).getChunkSnapshot());
						}
					}


					iterator.remove();
				}
			}
		}, 20L, 20L * 30);
	}

	public void scanChunk(String factionId, ChunkSnapshot snapshot)
	{
		List<ChunkSnapshot> snapshots = new ArrayList<>();

		if (getChunksToCheck().containsKey(factionId))
		{
			snapshots = getChunksToCheck().get(factionId);
		}

		for (ChunkSnapshot chunkSnapshot : snapshots)
		{
			if (chunkSnapshot.getX() == snapshot.getX() &&
					chunkSnapshot.getZ() == snapshot.getZ())
			{
				return;
			}
		}

		snapshots.add(snapshot);

		getChunksToCheck().put(factionId, snapshots);
	}

	public void run()
	{
		Iterator<Faction> iterator = factionsToCheck.iterator();

		int x   = 0;
		int max = plugin.getConfig().getInt("maxScan", 5);

        /* First scan. */

		while (iterator.hasNext())
		{
			if (x >= max)
			{
				break;
			}

			x++;

			Faction faction = iterator.next();

			if (faction == null)
			{
				iterator.remove();
				continue;
			}

			if (faction.isNormal())
			{
				if (faction.getTag().equalsIgnoreCase("SafeZone") ||
						faction.getTag().equalsIgnoreCase("Warzone"))
				{
					iterator.remove();
					continue;
				}

				final List<ChunkSnapshot> snapshots             = new ArrayList<>();
				final Set<Object>         forcedLoadedLocations = new HashSet<>();

				for (FLocation fLocation : Board.getFactionClaims(faction))
				{
					Chunk chunk = Bukkit.getWorld(fLocation.getWorldName()).getChunkAt((int) fLocation.getX(), (int) fLocation.getZ());

					if (!chunk.isLoaded())
					{
						forcedLoadedLocations.add(fLocation);
						chunk.load();
					}

					ChunkSnapshot snapshot = chunk.getChunkSnapshot();

					if (!snapshots.contains(snapshot))
					{
						snapshots.add(snapshot);
					}
				}

				iterator.remove();

				if (faction.getId() == null)
				{
					continue;
				}

				Bukkit.getScheduler().runTaskAsynchronously(plugin, () ->
				{
					FactionsResult factionsResult = new FactionsResult(plugin, faction.getId(), snapshots, (HashMap<Material, Double>) plugin.getWorths().clone(), new ConcurrentHashMap<>(plugin.getSpawnerWorths()));

					final LTResult fResult;

					fResult = new LTResult(plugin, faction.getId(), faction.getTag());

					fResult.update(factionsResult.getResult());

					Bukkit.getScheduler().runTask(plugin, () ->
					{
						int oldPosition = 0;

						for (LTResult result : results)
						{
							if (result.getId().equals(fResult.getId()))
							{
								oldPosition = result.getPosition();
							}
						}

						fResult.setPosition(oldPosition);

						results.removeIf(result -> result.getId().equalsIgnoreCase(fResult.getId()));
						results.add(fResult);

						Bukkit.getScheduler().runTaskLater(plugin, new BalancesRunnable(plugin, fResult), 5L);

						for (FLocation fLocation : Board.getFactionClaims(faction))
						{
							Chunk chunk = Bukkit.getWorld(fLocation.getWorldName()).getChunkAt((int) fLocation.getX(), (int) fLocation.getZ());

							if (forcedLoadedLocations.contains(fLocation))
							{
								chunk.unload();
							}
						}
					});
				});
			} else
			{
				iterator.remove();
			}
		}

        /* Scan by chunk */

		for (String fId : chunksToCheck.keySet())
		{
			final Faction faction = Factions.i.get(fId);

			if (faction == null)
			{
				continue;
			}

			List<ChunkSnapshot> snapshots = chunksToCheck.get(fId);

			boolean canProceed = false;

			for (LTResult fResult : results)
			{
				if (fResult.getId().equals(faction.getId()))
				{
					canProceed = true;
				}
			}

			if (!canProceed)
			{
				// First scan not done for this faction yet.
				if (!factionsToCheck.contains(faction))
				{
					factionsToCheck.add(faction);
				}

				continue;
			}

			Bukkit.getScheduler().runTaskAsynchronously(plugin, () ->
			{
				LTResult tResult = null;

				for (LTResult result : results)
				{
					if (result.getId().equals(faction.getId()))
					{
						tResult = result;
					}
				}

				if (tResult == null)
				{
					return;
				}

				final LTResult fResult = tResult;

				FactionsResult factionsResult = new FactionsResult(plugin, faction.getId(), snapshots, (HashMap<Material, Double>) plugin.getWorths().clone(), new ConcurrentHashMap<>(plugin.getSpawnerWorths()));

				fResult.update(factionsResult.getResult());

				Bukkit.getScheduler().runTask(plugin, () ->
				{
					int oldPosition = 0;

					for (LTResult result : results)
					{
						if (result.getId().equals(fResult.getId()))
						{
							oldPosition = result.getPosition();
						}
					}

					fResult.setPosition(oldPosition);

					results.removeIf(result -> result.getId().equalsIgnoreCase(fResult.getId()));
					results.add(fResult);

					Bukkit.getScheduler().runTaskLater(plugin, new BalancesRunnable(plugin, fResult), 5L);
				});
			});

			x++;
		}

		chunksToCheck.clear();

		if (plugin.getConfig().getBoolean("debug", false))
		{
			Bukkit.getLogger().info("[FactionsTop] Scanned " + x + " factions.");
		}

		results.removeIf(result -> getFaction(result.getId()) == null);

		final List<LTResult> results = new ArrayList<>(this.results);
		results.sort((o1, o2) -> (int) (o2.getWorth() - o1.getWorth()));
		this.sortedResults = results;

		int amountOfFactions = Factions.i.get().size();

		if (sortedResults.size() > (amountOfFactions - 5))
		{
			for (int y = 0; y < sortedResults.size(); y++)
			{
				LTResult result = sortedResults.get(y);

				if (y + 1 < result.getPosition() &&
						y < 5 &&
						result.getPosition() != 0)
				{
					plugin.getLocale().getMessage("broadcast").replace("{faction}", result.getName())
							.replace("{position}", "" + (y + 1)).broadcast();
				}

				result.setPosition(y + 1);
			}
		}
	}

	private Faction getFaction(String id)
	{
		return Factions.i.get(id);
	}

	public List<LTResult> getResults()
	{
		return sortedResults;
	}

	public String replace(CommandSender sender, int place, LTResult result, String string)
	{
		if (getFaction(result.getId()) == null)
		{
			return "";
		}

		string = string.replace("{number}", String.valueOf(place))
				.replace("{faction_leader}", result.getLeader())
				.replace("{worth}", formatNumber(result.getWorth()));

		if (sender instanceof Player)
		{
			string = string.replace("{faction_name}", Factions.i.get(result.getId()).getRelationTo(FPlayers.i.get((Player) sender)).getColor() + result.getName());
		} else
		{
			string = string.replace("{faction_name}", result.getName());
		}

		string = string.replace("{balance}", formatNumber(result.getBalances()))
				.replace("{faction_richest}", result.getRichest())
				.replace("{faction_total}", formatNumber(result.getBalances() + result.getWorth()));

		for (Material type : result.getAmount().keySet())
		{
			if (result.getAmount().get(type) == 0 && string.toLowerCase().contains("{material:" + type.name().toLowerCase() + "}"))
			{
				string = "";
			}

			string = string.replace("{material:" + type.name().toLowerCase() + "}", formatNumber(result.getAmount().get(type)));
		}

		for (String creature : result.getCreatureAmount().keySet())
		{
			if (result.getCreatureAmount().get(creature) == 0 && string.toLowerCase().contains("{spawner:" + creature.toLowerCase() + "}"))
			{
				string = "";
			}

			string = string.replace("{spawner:" + creature.toLowerCase() + "}", formatNumber(result.getCreatureAmount().get(creature)));
		}

		return string;
	}

}