package xyz.boomclaw.plugins.factionstop.versions.two_nine;

import com.massivecraft.factions.entity.FactionColl;
import com.massivecraft.factions.entity.MPlayer;
import xyz.boomclaw.plugins.factionstop.FactionsTop;
import xyz.boomclaw.plugins.factionstop.models.LTResult;

public class BalancesRunnable implements Runnable
{

	private FactionsTop plugin;
	private LTResult    result;

	public BalancesRunnable(FactionsTop plugin, LTResult result)
	{
		this.plugin = plugin;
		this.result = result;
	}

	@Override
	public void run()
	{
		int    balance    = 0;
		double currentMax = Integer.MIN_VALUE;
		String richest    = "";

		if (plugin.getEconomy() != null)
		{
			if (FactionColl.get().get(result.getId()) == null)
			{
				return;
			}

			for (MPlayer player : FactionColl.get().get(result.getId()).getMPlayers())
			{
				double balanceTemp = plugin.getEconomy().getBalance(player.getName());

				if (balanceTemp > currentMax)
				{
					richest = player.getName();
					currentMax = balanceTemp;
				}

				balance += balanceTemp;
			}
		}

		result.setRichest(richest);
		result.setBalances(balance);
	}

}