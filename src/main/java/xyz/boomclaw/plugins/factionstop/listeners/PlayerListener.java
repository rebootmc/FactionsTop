package xyz.boomclaw.plugins.factionstop.listeners;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.entity.BoardColl;
import com.massivecraft.massivecore.ps.PS;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.ChunkSnapshot;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import xyz.boomclaw.plugins.factionstop.FactionsTop;
import xyz.boomclaw.plugins.factionstop.models.LTResult;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

public class PlayerListener implements Listener
{

	private FactionsTop plugin;

	private HashMap<String, Integer> signCooldown = new HashMap<>();

	public PlayerListener(FactionsTop plugin)
	{
		this.plugin = plugin;

		Bukkit.getScheduler().runTaskTimer(plugin, () ->
		{
			Iterator iterator = signCooldown.keySet().iterator();

			while (iterator.hasNext())
			{
				String player = (String) iterator.next();

				int seconds = signCooldown.get(player);
				seconds--;

				if (seconds == 0)
				{
					iterator.remove();
				} else
				{
					signCooldown.put(player, seconds);
				}
			}
		}, 20L, 20L);
	}

	@EventHandler
	public void onPlayerPlace(BlockPlaceEvent event)
	{
		if (plugin.getFVersion() == FactionsTop.FVersion.ONE_EIGHT)
		{
			if (Board.getFactionAt(event.getBlock()) == null)
			{
				return;
			}

			if (!Board.getFactionAt(event.getBlock()).isNormal())
			{
				return;
			}
		} else
		{
			if (BoardColl.get().getFactionAt(PS.valueOf(event.getBlock())) == null)
			{
				return;
			}

			if (!BoardColl.get().getFactionAt(PS.valueOf(event.getBlock())).isNormal())
			{
				return;
			}
		}

		check(event.getBlockPlaced(), true);
	}

	private void check(final Block block, boolean place)
	{
		if (block.getType() == Material.MOB_SPAWNER || plugin.getConfig().isSet("blocks." + block.getType().name().toLowerCase()))
		{
			Bukkit.getScheduler().runTaskLater(plugin, () -> scanLocation(block.getLocation()), new Random().nextInt(5) * 20L + 5L);
		}
	}

	public void scanLocation(Location location)
	{
		if (plugin.getFVersion() == FactionsTop.FVersion.ONE_EIGHT)
		{
			com.massivecraft.factions.Faction faction = Board.getFactionAt(location);

			if (!faction.getId().equalsIgnoreCase("0"))
			{
				ChunkSnapshot snapshot = location.getChunk().getChunkSnapshot();

				plugin.getManager().scanChunk(faction.getId(), snapshot);
			}
		} else
		{
			com.massivecraft.factions.entity.Faction faction = BoardColl.get().getFactionAt(PS.valueOf(location));

			if (!faction.getId().equalsIgnoreCase("0"))
			{
				ChunkSnapshot snapshot = location.getChunk().getChunkSnapshot();

				plugin.getManager().scanChunk(faction.getId(), snapshot);
			}
		}
	}

	@EventHandler
	public void onPlayerBreak(BlockBreakEvent event)
	{
		if (plugin.getFVersion() == FactionsTop.FVersion.ONE_EIGHT)
		{
			if (Board.getFactionAt(event.getBlock()) == null)
			{
				return;
			}

			if (!Board.getFactionAt(event.getBlock()).isNormal())
			{
				return;
			}
		} else
		{
			if (BoardColl.get().getFactionAt(PS.valueOf(event.getBlock())) == null)
			{
				return;
			}

			if (!BoardColl.get().getFactionAt(PS.valueOf(event.getBlock())).isNormal())
			{
				return;
			}
		}

		check(event.getBlock(), false);
	}

	@EventHandler
	public void onPlayerCommand(PlayerCommandPreprocessEvent event)
	{
		if (event.getMessage().startsWith("/f top"))
		{
			event.getPlayer().performCommand(event.getMessage().replace("/f top", "ftop"));
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onPlayerJoin(final PlayerJoinEvent event)
	{
		Bukkit.getScheduler().runTaskLater(plugin, () -> plugin.getRunnable().updateIndp(event.getPlayer()), 5L);
	}

	@EventHandler
	public void onPlayerTeleport(final PlayerTeleportEvent event)
	{
		Bukkit.getScheduler().runTaskLater(plugin, () -> plugin.getRunnable().updateIndp(event.getPlayer()), 5L);
	}

	@EventHandler
	public void onSignChange(final SignChangeEvent event)
	{
		if (event.getLine(0).equalsIgnoreCase("[FTop]"))
		{
			if (!event.getPlayer().hasPermission("factionstop.createsign"))
			{
				plugin.getLocale().getMessage("noPerm").send(event.getPlayer());
				event.setCancelled(true);
			} else
			{
				event.getPlayer().sendMessage(ChatColor.YELLOW + "Sign created. Please allow up to 30 seconds for it to update.");

				boolean exists = false;

				for (Location location : plugin.getRunnable().getSignLocations())
				{
					if (location.getBlockX() == event.getBlock().getLocation().getBlockX() &&
							location.getBlockY() == event.getBlock().getLocation().getBlockY() &&
							location.getBlockZ() == event.getBlock().getLocation().getBlockZ())
					{
						exists = true;
					}
				}

				if (!exists) plugin.getRunnable().getSignLocations().add(event.getBlock().getLocation());
			}
		}
	}

	@EventHandler
	public void onSignClick(final PlayerInteractEvent event)
	{
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK)
		{
			return;
		}

		if (event.getClickedBlock().getState() instanceof Sign)
		{
			Sign sign = (Sign) event.getClickedBlock().getState();

			if (sign.getLine(0).equalsIgnoreCase("[FTop]"))
			{
				if (signCooldown.containsKey(event.getPlayer().getName()))
				{
					plugin.getLocale().getMessage("sign.cooldown").replace("{seconds}", String.valueOf(signCooldown.get(event.getPlayer().getName())))
							.send(event.getPlayer());

					event.setCancelled(true);
					return;
				}

				int place = Integer.parseInt(sign.getLine(1));

				if (plugin.getManager().getResults().size() >= place)
				{
					LTResult result = plugin.getManager().getResults().get(place - 1);

					event.getPlayer().performCommand("f who " + result.getName());
				}

				signCooldown.put(event.getPlayer().getName(), plugin.getConfig().getInt("signcooldown", 3));
				event.setCancelled(true);
			}
		}
	}

}