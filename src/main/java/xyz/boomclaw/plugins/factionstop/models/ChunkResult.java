package xyz.boomclaw.plugins.factionstop.models;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.ChunkSnapshot;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import uk.antiperson.stackspawner.StackSpawner;
import xyz.boomclaw.plugins.factionstop.FactionsTop;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class ChunkResult
{

	private ChunkSnapshot snapshot;
	private FactionsTop   plugin;
	@Getter
	private int           x;
	@Getter
	private int           z;

	@Getter
	private String factionId;

	private HashMap<Material, Double>         worths        = new HashMap<>();
	private ConcurrentHashMap<String, Double> spawnerWorths = new ConcurrentHashMap<>();

	@Getter
	private HashMap<Material, Integer> amount         = new HashMap<>();
	@Getter
	private HashMap<String, Integer>   creatureAmount = new HashMap<>();
	@Getter
	@Setter
	private double                     worth          = 0;

	public ChunkResult(FactionsTop plugin, String factionId, ChunkSnapshot snapshot, HashMap<Material, Double> worths, ConcurrentHashMap<String, Double> spawnerWorths)
	{
		this.plugin = plugin;
		this.factionId = factionId;
		this.snapshot = snapshot;
		this.worths = worths;
		this.spawnerWorths = spawnerWorths;

		this.x = snapshot.getX();
		this.z = snapshot.getZ();
	}

	public void calculate()
	{
		double worth = 0;

		for (int x = 0; x < 16; ++x)
		{
			for (int y = 0; y < 256 && !snapshot.isSectionEmpty(y >> 4); ++y)
			{
				for (int z = 0; z < 16; ++z)
				{
					if (snapshot.getBlockTypeId(x, y, z) == 52)
					{
						try
						{
							final Block block = Bukkit.getWorld(snapshot.getWorldName()).getBlockAt(snapshot.getX() * 16 + x, y, snapshot.getZ() * 16 + z);

							if (block.getType() == Material.MOB_SPAWNER)
							{
								int amount = 1;

								if (plugin.isSSEnabled())
								{
									StackSpawner stackSpawner = (StackSpawner) plugin.getServer().getPluginManager().getPlugin("StackSpawner");

									for (Location loc : stackSpawner.spawnerAmount.keySet())
									{
										if (loc.getX() == (snapshot.getX() * 16 + x) &&
												loc.getY() == y &&
												loc.getZ() == (snapshot.getZ() * 16 + z))
										{
											amount = stackSpawner.spawnerAmount.get(loc);
											break;
										}
									}
								}

								final CreatureSpawner spawner = (CreatureSpawner) block.getState();
								getCreatureAmount().put(spawner.getSpawnedType().name().toLowerCase(),
										getCreatureAmount().containsKey(spawner.getSpawnedType().name().toLowerCase()) ? (
												getCreatureAmount().get(spawner.getSpawnedType().name().toLowerCase()) + amount
										) : amount);
							}
						} catch (Throwable t)
						{
							plugin.getManager().scanChunk(factionId, snapshot);
						}
					} else
					{
						Material material = Material.getMaterial(snapshot.getBlockTypeId(x, y, z));
						if (worths.containsKey(material))
						{
							double nWorth = worths.get(material);
							worth += nWorth;

							getAmount().put(material,
									getAmount().containsKey(material) ? (
											getAmount().get(material) + 1
									) : 1);
						}
					}
				}
			}
		}

		// calculate spawner worth
		for (String creature : getCreatureAmount().keySet())
		{
			if (spawnerWorths.containsKey(creature))
			{
				worth += spawnerWorths.get(creature) * getCreatureAmount().get(creature);
			}
		}

		this.worth = worth;
		this.worths = null;
		this.spawnerWorths = null;
		this.snapshot = null;
		this.plugin = null;
	}

}