package xyz.boomclaw.plugins.factionstop.models;

import org.bukkit.ChunkSnapshot;
import org.bukkit.Material;
import xyz.boomclaw.plugins.factionstop.FactionsTop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class FactionsResult
{

	private FactionsTop         plugin;
	private List<ChunkSnapshot> snapshots;
	private String              factionId;

	private HashMap<Material, Double>         worths        = new HashMap<>();
	private ConcurrentHashMap<String, Double> spawnerWorths = new ConcurrentHashMap<>();

	public FactionsResult(FactionsTop plugin, String factionId, List<ChunkSnapshot> snapshots, HashMap<Material, Double> worths, ConcurrentHashMap<String, Double> spawnerWorths)
	{
		this.plugin = plugin;
		this.factionId = factionId;
		this.snapshots = snapshots;
		this.worths = worths;
		this.spawnerWorths = spawnerWorths;
	}

	public List<ChunkResult> getResult()
	{
		List<ChunkResult> results = new ArrayList<>();

		for (final ChunkSnapshot snapshot : snapshots)
		{
			ChunkResult chunkResult = new ChunkResult(plugin, factionId, snapshot, worths, spawnerWorths);

			chunkResult.calculate();

			results.add(chunkResult);
		}

		return results;
	}

}