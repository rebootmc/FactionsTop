package xyz.boomclaw.plugins.factionstop.models;

import com.massivecraft.factions.Factions;
import com.massivecraft.factions.entity.FactionColl;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Material;
import xyz.boomclaw.plugins.factionstop.FactionsTop;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LTResult
{

	private FactionsTop plugin;
	@Getter
	private String      name;
	@Getter
	private String      id;
	@Getter
	private HashMap<Material, Integer> amount         = new HashMap<>();
	@Getter
	private HashMap<String, Integer>   creatureAmount = new HashMap<>();
	@Getter
	@Setter
	private double                     worth          = 0;
	@Getter
	@Setter
	private String                     richest        = "";
	@Getter
	@Setter
	private String                     leader         = "";
	@Getter
	@Setter
	private int                        balances       = 0;
	@Getter
	@Setter
	private int                        position       = 0;
	@Getter
	private List<ChunkResult>          chunkResults   = new ArrayList<>();

	public LTResult(FactionsTop plugin, String id, String name)
	{
		this.plugin = plugin;
		this.id = id;
		this.name = name;

		updateLeader();
	}

	private void updateLeader()
	{
		if (plugin.getFVersion() == FactionsTop.FVersion.ONE_EIGHT)
		{
			this.leader = Factions.i.get(id).getFPlayerLeader().getName();
		} else
		{
			this.leader = FactionColl.get().get(id).getLeader().getName();
		}
	}

	public void update(List<ChunkResult> results)
	{
		for (ChunkResult newChunkResult : results)
		{
			chunkResults.removeIf(result -> result.getX() == newChunkResult.getX() && result.getZ() == newChunkResult.getZ());

			chunkResults.add(newChunkResult);
		}

		updateLeader();

		calculate();
	}

	public void calculate()
	{
		this.worth = 0;
		this.amount.clear();
		this.creatureAmount.clear();

		for (ChunkResult result : chunkResults)
		{
			this.worth += result.getWorth();

			for (Material mat : result.getAmount().keySet())
			{
				amount.put(mat, amount.containsKey(mat) ? amount.get(mat) + result.getAmount().get(mat) : result.getAmount().get(mat));
			}

			for (String creature : result.getCreatureAmount().keySet())
			{
				creatureAmount.put(creature, creatureAmount.containsKey(creature) ? creatureAmount.get(creature) + result.getCreatureAmount().get(creature) : result.getCreatureAmount().get(creature));
			}
		}

		for (String material : plugin.getConfig().getConfigurationSection("blocks").getKeys(false))
		{
			try
			{
				Material mat = Material.getMaterial(material.toUpperCase());

				if (mat != null)
				{
					if (!getAmount().containsKey(mat))
					{
						getAmount().put(mat, 0);
					}
				}
			} catch (Exception e)
			{
				// No material (e.g. error in config)
			}
		}

		for (String creature : plugin.getConfig().getConfigurationSection("spawners").getKeys(false))
		{
			if (!getCreatureAmount().containsKey(creature))
			{
				getCreatureAmount().put(creature, 0);
			}
		}
	}

}