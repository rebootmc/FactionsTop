package xyz.boomclaw.plugins.factionstop.data;

import lombok.Getter;
import xyz.boomclaw.plugins.factionstop.FactionsTop;
import xyz.boomclaw.plugins.factionstop.locale.Configuration;

public class StorageData
{

	@Getter
	private Configuration config;

	public StorageData(FactionsTop plugin)
	{
		config = new Configuration(plugin, "storage");
	}

	public void save()
	{
		config.save();
	}

}