package xyz.boomclaw.plugins.factionstop;

import lombok.Getter;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import xyz.boomclaw.plugins.factionstop.commands.FTopCommand;
import xyz.boomclaw.plugins.factionstop.data.StorageData;
import xyz.boomclaw.plugins.factionstop.listeners.PlayerListener;
import xyz.boomclaw.plugins.factionstop.locale.DefaultConfiguration;
import xyz.boomclaw.plugins.factionstop.locale.Locale;
import xyz.boomclaw.plugins.factionstop.manager.FactionsManager;
import xyz.boomclaw.plugins.factionstop.runnables.SignRunnable;
import xyz.boomclaw.plugins.factionstop.utils.RawUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class FactionsTop extends JavaPlugin
{

	@Getter
	private FVersion        fVersion;
	@Getter
	private Locale          locale;
	@Getter
	private FactionsManager manager;
	@Getter
	private Economy economy = null;
	@Getter
	private SignRunnable runnable;
	@Getter
	private HashMap<Material, Double>         worths        = new HashMap<>();
	@Getter
	private ConcurrentHashMap<String, Double> spawnerWorths = new ConcurrentHashMap<>();
	@Getter
	private StorageData    storageData;
	@Getter
	private PlayerListener playerListener;

	@Override
	public void onEnable()
	{
		if (!Bukkit.getPluginManager().isPluginEnabled("Factions"))
		{
			Bukkit.getLogger().severe("[FactionsTop] Missing dependency: Factions");
			return;
		}

		fVersion = FVersion.TWO_NINE;

		if (Bukkit.getPluginManager().getPlugin("Factions").getDescription().getVersion().startsWith("1.8"))
		{
			getLogger().info(Bukkit.getPluginManager().getPlugin("Factions").getDescription().getVersion() + " detected. Using 1.8 API hooks");
			fVersion = FVersion.ONE_EIGHT;
		} else
		{
			getLogger().info(Bukkit.getPluginManager().getPlugin("Factions").getDescription().getVersion() + " detected. Using 2.9 API hooks.");
		}

		if (Bukkit.getPluginManager().isPluginEnabled("Vault"))
		{
			setupEconomy();
		}

		this.locale = new Locale(this);

		this.locale.loadDefault(new DefaultConfiguration()
		{
			@Override
			public void insertValues()
			{
				insert("command.header", "&e&lTop Factions (page {page}/{max}):");
				insert("command.results", "&e&l#{number} &b{faction_name} &a${worth}");
				insert("sign.cooldown", "&cPlease wait {seconds} seconds.");
				insert("broadcast", "&e{faction}&a is now position &e&l{position}&a.");
				insert("noPerm", "&cYou do not have permission.");
			}
		});

		saveDefaultConfig();

		storageData = new StorageData(this);

		if (fVersion == FVersion.ONE_EIGHT)
		{
			this.manager = new xyz.boomclaw.plugins.factionstop.versions.one_eight.FactionsManager(this);
		} else
		{
			this.manager = new xyz.boomclaw.plugins.factionstop.versions.two_nine.FactionsManager(this);
		}

		getServer().getPluginManager().registerEvents(playerListener = new PlayerListener(this), this);
		getServer().getPluginManager().addPermission(new Permission("factionstop.view", PermissionDefault.TRUE));
		getServer().getPluginManager().addPermission(new Permission("factionstop.createsign", PermissionDefault.OP));
		getServer().getPluginManager().addPermission(new Permission("factionstop.debug", PermissionDefault.OP));
		getServer().getPluginManager().addPermission(new Permission("factionstop.reload", PermissionDefault.OP));

		getCommand("ftop").setExecutor(new FTopCommand(this));

		getServer().getScheduler().runTaskTimer(this, runnable = new SignRunnable(this), 20 * 5, 20 * 25);

		for (String locString : storageData.getConfig().getConfig().getStringList("signs"))
		{
			runnable.getSignLocations().add(RawUtils.stringToLoc(locString));
		}

		for (String material : getConfig().getConfigurationSection("blocks").getKeys(false))
		{
			try
			{
				worths.put(Material.valueOf(material.toUpperCase()), getConfig().getDouble("blocks." + material, 0));
			} catch (Exception e)
			{
				Bukkit.getLogger().severe("[FactionsTop] Unable to recognise material: " + material.toUpperCase());
			}
		}

		for (String name : getConfig().getConfigurationSection("spawners").getKeys(false))
		{
			spawnerWorths.put(name.toLowerCase(), getConfig().getDouble("spawners." + name, 0));
		}
	}

	private boolean setupEconomy()
	{
		RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null)
		{
			economy = economyProvider.getProvider();
		}

		return (economy != null);
	}

	@Override
	public void onDisable()
	{
		List<String> locations = new ArrayList<>();

		for (Location location : runnable.getSignLocations())
		{
			locations.add(RawUtils.locToString(location));
		}

		storageData.getConfig().getConfig().set("signs", locations);
		storageData.save();
	}

	public boolean isSSEnabled()
	{
		return getServer().getPluginManager().isPluginEnabled("StackSpawner");
	}

	public enum FVersion
	{

		ONE_EIGHT,
		TWO_NINE

	}

}