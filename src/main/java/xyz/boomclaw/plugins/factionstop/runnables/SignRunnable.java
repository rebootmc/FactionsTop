package xyz.boomclaw.plugins.factionstop.runnables;

import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import xyz.boomclaw.plugins.factionstop.FactionsTop;
import xyz.boomclaw.plugins.factionstop.models.LTResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class SignRunnable implements Runnable
{

	private FactionsTop              plugin;
	private HashMap<String, Integer> recentlyUpdated;
	@Getter
	private List<Location>           signLocations;

	private int scanTime = 6;

	public SignRunnable(FactionsTop plugin)
	{
		this.plugin = plugin;
		this.recentlyUpdated = new HashMap<>();
		this.signLocations = new ArrayList<>();

		Bukkit.getScheduler().runTaskTimer(plugin, () ->
		{
			Iterator iterator = recentlyUpdated.keySet().iterator();

			while (iterator.hasNext())
			{
				String player = (String) iterator.next();

				int seconds = recentlyUpdated.get(player);
				seconds--;

				if (seconds == 0)
				{
					iterator.remove();
				} else
				{
					recentlyUpdated.put(player, seconds);
				}
			}
		}, 20L, 20L);
	}

	@Override
	public void run()
	{
		scanTime++;

		if (scanTime == 7)
		{
			for (World world : Bukkit.getWorlds())
			{
				for (final Chunk chunk : world.getLoadedChunks())
				{
					for (BlockState entity : chunk.getTileEntities())
					{
						if (entity instanceof Sign)
						{
							Sign sign = (Sign) entity;

							if (!sign.getLine(0).equalsIgnoreCase("[FTop]"))
							{
								continue;
							}

							boolean exists = false;

							for (Location location : signLocations)
							{
								if (location.getBlockX() == sign.getLocation().getBlockX() &&
										location.getBlockY() == sign.getLocation().getBlockY() &&
										location.getBlockZ() == sign.getLocation().getBlockZ())
								{
									exists = true;
								}
							}

							if (!exists) signLocations.add(sign.getLocation());
						}
					}
				}
			}

			scanTime = 0;
		}

		Iterator iterator = signLocations.iterator();

		while (iterator.hasNext())
		{
			Location location = (Location) iterator.next();

			if (location.getBlock().getState() instanceof Sign)
			{
				Sign sign = (Sign) location.getBlock().getState();

				if (!sign.getLine(0).equalsIgnoreCase("[FTop]"))
				{
					iterator.remove();
					continue;
				}

				for (Player player : Bukkit.getOnlinePlayers())
				{
					update(player, sign);
				}
			} else
			{
				iterator.remove();
			}
		}
	}

	public void update(Player player, Sign sign)
	{
		if (!player.getWorld().getName().equals(sign.getLocation().getWorld().getName()))
		{
			return;
		}

		if (player.getLocation().distance(sign.getLocation()) > 32)
		{
			return;
		}

		int place = 1;

		try
		{
			place = Integer.valueOf(sign.getLine(1));
		} catch (Exception e)
		{
			place = 1;
		}

		if (plugin.getManager().getResults().size() < place)
		{
			player.sendSignChange(sign.getLocation(), new String[4]);
			return;
		}

		LTResult result = plugin.getManager().getResults().get(place - 1);

		List<String> messages = new ArrayList<>();

		for (String message : plugin.getConfig().getStringList("sign"))
		{
			messages.add(ChatColor.translateAlternateColorCodes('&', plugin.getManager().replace(player, place, result, message)));
		}

		player.sendSignChange(sign.getLocation(), messages.toArray(new String[messages.size()]));
		recentlyUpdated.put(player.getName(), 15);
	}

	public void updateIndp(Player player)
	{
		for (Location location : signLocations)
		{
			if (location.getBlock().getState() instanceof Sign)
			{
				Sign sign = (Sign) location.getBlock().getState();

				if (!sign.getLine(0).equalsIgnoreCase("[FTop]"))
				{
					continue;
				}

				update(player, sign);
			}
		}
	}

}