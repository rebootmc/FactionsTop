package xyz.boomclaw.plugins.factionstop.locale;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

public class Configuration
{

	@Getter
	protected final String            name;
	@Getter
	private final   YamlConfiguration config;
	private final   File              file;
	protected       Plugin            plugin;

	public Configuration(Plugin plugin, String name)
	{
		this.plugin = plugin;
		this.name = name;

		this.file = new File(plugin.getDataFolder(), name + ".yml");

		if (!file.exists())
		{
			file.getParentFile().mkdirs();

			try
			{
				file.createNewFile();
			} catch (IOException e)
			{
				Bukkit.getLogger().severe("[FactionsTop] Unable to create file: " + name + ".yml");
			}
		}

		this.config = YamlConfiguration.loadConfiguration(file);
	}

	public void loadDefault(DefaultConfiguration defaultConfig)
	{
		boolean changes = false;

		for (String key : defaultConfig.getDefaultValues().keySet())
		{
			if (config.isSet(key))
			{
				continue;
			}

			config.set(key, defaultConfig.getDefaultValues().get(key));
			Bukkit.getLogger().info("[FactionsTop] Loaded default value of " + key + " into " + name + ".yml");

			changes = true;
		}

		if (changes)
		{
			save();
		}
	}

	public void save()
	{
		try
		{
			config.save(file);
		} catch (IOException e)
		{
			Bukkit.getLogger().severe("[FactionsTop] Unable to save to file: " + name + ".yml");
		}
	}

}