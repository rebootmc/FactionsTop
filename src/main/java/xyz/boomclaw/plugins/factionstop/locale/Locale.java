package xyz.boomclaw.plugins.factionstop.locale;


import org.bukkit.plugin.Plugin;

public class Locale extends Configuration
{

	public Locale(Plugin plugin)
	{
		super(plugin, "messages");
	}

	public Locale(Plugin plugin, String name)
	{
		super(plugin, name);
	}

	public Message getMessage(String key)
	{
		return new Message(key, getConfig().getString(key, ""));
	}

}