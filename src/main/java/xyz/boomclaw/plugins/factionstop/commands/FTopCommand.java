package xyz.boomclaw.plugins.factionstop.commands;

import mkremins.fanciful.FancyMessage;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import xyz.boomclaw.plugins.factionstop.FactionsTop;
import xyz.boomclaw.plugins.factionstop.models.LTResult;

import java.util.ArrayList;
import java.util.List;

public class FTopCommand implements CommandExecutor
{

	private FactionsTop plugin;

	public FTopCommand(FactionsTop plugin)
	{
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args)
	{
		if (command.getName().equalsIgnoreCase("ftop"))
		{
			if (args.length > 0)
			{
				if (args[0].equalsIgnoreCase("debug"))
				{
					if (commandSender.hasPermission("factionstop.debug"))
					{
						boolean currentDebug = plugin.getConfig().getBoolean("debug", false);

						if (!currentDebug)
						{
							plugin.getConfig().set("debug", true);
							commandSender.sendMessage(ChatColor.YELLOW + "Debug mode toggled " + ChatColor.GREEN + "on" + ChatColor.YELLOW + ".");
						} else
						{
							plugin.getConfig().set("debug", false);
							commandSender.sendMessage(ChatColor.YELLOW + "Debug mode toggled " + ChatColor.RED + "off" + ChatColor.YELLOW + ".");
						}

						plugin.saveConfig();

						return true;
					} else
					{
						plugin.getLocale().getMessage("noPerm").send(commandSender);
						return true;
					}
				} else if (args[0].equalsIgnoreCase("reload"))
				{
					if (commandSender.hasPermission("factionstop.reload"))
					{
						plugin.reloadConfig();
						commandSender.sendMessage(ChatColor.YELLOW + "Configuration reloaded.");

						return true;
					} else
					{
						plugin.getLocale().getMessage("noPerm").send(commandSender);
						return true;
					}
				}
			}

			int page = 1;

			if (args.length > 0)
			{
				try
				{
					page = Integer.valueOf(args[0]);
				} catch (Exception e)
				{
					page = 1;
				}
			}

			if (page < 1)
			{
				page = 1;
			}

			List<LTResult> results        = plugin.getManager().getResults();
			int            max            = plugin.getConfig().getInt("list-no", 10);
			int            pagesAvailable = (int) Math.ceil((double) results.size() / (double) max);

			plugin.getLocale().getMessage("command.header").replace("{page}", String.valueOf(page))
					.replace("{max}", String.valueOf(pagesAvailable)).send(commandSender);

			for (int x = (page * max - (max)); x < (page * max); x++)
			{
				if (results.size() - 1 < x)
				{
					return true;
				}

				if (results.get(x) == null)
				{
					x--;
					continue;
				}

				List<String> hoverMessages = new ArrayList<>();

				for (String message : plugin.getConfig().getStringList("hover"))
				{
					String aMessage = ChatColor.translateAlternateColorCodes('&', plugin.getManager().replace(commandSender, x + 1, results.get(x), message));

					if (!aMessage.equalsIgnoreCase(""))
					{
						hoverMessages.add(aMessage);
					}
				}
				new FancyMessage(plugin.getManager().replace(commandSender, x + 1, results.get(x), plugin.getLocale().getMessage("command.results").get())).tooltip(hoverMessages).send(commandSender);
			}

			return true;
		}

		return false;
	}

}