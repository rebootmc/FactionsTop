package xyz.boomclaw.plugins.factionstop.manager;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.ChunkSnapshot;
import org.bukkit.command.CommandSender;
import xyz.boomclaw.plugins.factionstop.FactionsTop;
import xyz.boomclaw.plugins.factionstop.models.LTResult;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class FactionsManager implements Runnable
{

	protected FactionsTop plugin;

	@Getter
	protected HashMap<String, List<ChunkSnapshot>> chunksToCheck = new HashMap<>();
	protected List<LTResult>                       results       = new ArrayList<>();
	protected List<LTResult>                       sortedResults = new ArrayList<>();
	protected DecimalFormat                        format        = new DecimalFormat("####,###,###");

	public FactionsManager(FactionsTop plugin)
	{
		this.plugin = plugin;

		Bukkit.getLogger().info("[FactionsTop] Running first scan. The results will be inaccurate for several minutes.");

		Bukkit.getScheduler().runTaskTimer(plugin, this, 1, plugin.getConfig().getInt("refresh", 5) * 20);
	}

	public void scanChunk(String factionId, ChunkSnapshot snapshot)
	{
		List<ChunkSnapshot> snapshots = new ArrayList<>();

		if (getChunksToCheck().containsKey(factionId))
		{
			snapshots = getChunksToCheck().get(factionId);
		}

		for (ChunkSnapshot chunkSnapshot : snapshots)
		{
			if (chunkSnapshot.getX() == snapshot.getX() &&
					chunkSnapshot.getZ() == snapshot.getZ())
			{
				return;
			}
		}

		snapshots.add(snapshot);

		getChunksToCheck().put(factionId, snapshots);
	}

	public abstract void run();

	public abstract String replace(CommandSender sender, int place, LTResult result, String string);

	public List<LTResult> getResults()
	{
		return sortedResults;
	}

	protected String formatNumber(double number)
	{
		return format.format(number);
	}

}