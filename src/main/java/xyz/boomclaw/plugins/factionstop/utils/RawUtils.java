package xyz.boomclaw.plugins.factionstop.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public class RawUtils
{

	public static String locToString(Location location)
	{
		return location.getWorld().getName() + ":" +
				location.getBlockX() + ":" +
				location.getBlockY() + ":" +
				location.getBlockZ();
	}

	public static Location stringToLoc(String rawLocation)
	{
		String[] parts = rawLocation.split(":");

		World world = Bukkit.getWorld(parts[0]);

		if (world == null)
		{
			return null;
		}

		int x = Integer.valueOf(parts[1]);
		int y = Integer.valueOf(parts[2]);
		int z = Integer.valueOf(parts[3]);

		return new Location(world, x, y, z);
	}

}